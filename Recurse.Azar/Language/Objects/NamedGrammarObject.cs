﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model;
using Recurse.Common.Text;

namespace Recurse.Azar.Language.Objects
{
    public class NamedGrammarObject : IGrammarObject
    {
        private readonly string _text;

        private readonly object _target;

        private readonly IGrammarAlias _pronoun;

        private readonly AliasMapping _aliases;

        public Hyperstring Adjective { get; set; }

        public NamedGrammarObject(AliasMapping aliases, string text, IGrammarAlias pronoun, object target)
        {
            _aliases = aliases;
            _pronoun = pronoun;
            _target = target;
            _text = text;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            if (_aliases != null && _pronoun != null)
            {
                if (Equals(_aliases.GetTarget(_pronoun), _target))
                    return _pronoun.GetNoun(type);
                _aliases.Add(_pronoun, _target);
            }
            return
                (Adjective ?? Hyperstring.Empty) +
                new ReferenceSymbol(_target, Equals(type, PronounType.Their) ? _text + "'s" : _text);
        }
    }
}