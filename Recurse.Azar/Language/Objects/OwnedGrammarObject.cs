﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language.Verbs;
using System;

namespace Recurse.Azar.Language.Objects
{
    public class OwnedGrammarObject : IEquatable<OwnedGrammarObject>, IGrammarAlias
    {
        private readonly IGrammarObject _owner;

        private readonly GrammarNoun _relation;

        public double Specificity
        {
            get { return 1; }
        }

        public OwnedGrammarObject(IGrammarObject owner, GrammarNoun relation)
        {
            _owner = owner;
            _relation = relation;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            var ownerNoun = _owner.GetNoun(PronounType.Their);
            return new GrammarNoun(
                ownerNoun.Hyperstring + _relation.Hyperstring,
                _relation.IsPlural);
        }

        public bool Equals(OwnedGrammarObject other)
        {
            return
                other != null &&
                Equals(_owner, other._owner) &&
                Equals(_relation, other._relation);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as OwnedGrammarObject);
        }

        public override int GetHashCode()
        {
            return _owner.GetHashCode() + _relation.GetHashCode();
        }
    }
}
