﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Language.Objects
{
    public class DescribedThing : IGrammarObject
    {
        private readonly IGrammarObject _thing;

        private readonly Hyperstring _adjective;

        public DescribedThing(IGrammarObject thing, Hyperstring adjective)
        {
            _thing = thing;
            _adjective = adjective;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            var noun = _thing.GetNoun(type);
            return new GrammarNoun(_adjective + noun.Hyperstring, noun.IsPlural);
        }
    }
}
