﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language.Verbs;
using System;

namespace Recurse.Azar.Language.Objects
{
    public class TitledGrammarObject : IEquatable<TitledGrammarObject>, IGrammarAlias
    {
        private readonly IGrammarObject _title, _target;

        public double Specificity
        {
            get { return 1; }
        }

        public TitledGrammarObject(IGrammarObject title, IGrammarObject target)
        {
            _title = title;
            _target = target;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            var titleNoun = _title.GetNoun(PronounType.Their);
            var targetNoun = _target.GetNoun(type);
            return new GrammarNoun(
                titleNoun.Hyperstring + targetNoun.Hyperstring,
                targetNoun.IsPlural);
        }

        public bool Equals(TitledGrammarObject other)
        {
            return
                other != null &&
                Equals(_title, other._title) &&
                Equals(_target, other._target);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TitledGrammarObject);
        }

        public override int GetHashCode()
        {
            return _title.GetHashCode() + _target.GetHashCode();
        }
    }
}
