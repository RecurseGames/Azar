﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Language
{
    public abstract class SimpleGrammarAction : GrammarAction
    {
        protected override Hyperstring GetSequence(
            IGrammarObject topic,
            IGrammarObject focus,
            GrammarCategory category)
        {
            var result =
                topic.GetNoun(PronounType.They).Hyperstring +
                GetVerb(category, focus != null);
            if (focus != null)
                result += focus.GetNoun(PronounType.Them).Hyperstring;
            return result;
        }
        
        protected abstract Hyperstring GetVerb(
            GrammarCategory category,
            bool isTransitive);
    }
}
