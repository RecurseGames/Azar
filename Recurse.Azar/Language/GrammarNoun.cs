﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;

namespace Recurse.Azar.Language
{
    public class GrammarNoun
    {
        public readonly Hyperstring Hyperstring;

        public readonly bool IsPlural;

        public GrammarNoun(Hyperstring hyperstring, bool isPlural)
        {
            Hyperstring = hyperstring;
            IsPlural = isPlural;
        }

        public static implicit operator GrammarNoun(Hyperstring hyperstring)
        {
            return new GrammarNoun(hyperstring, false);
        }

        public static implicit operator GrammarNoun(string text)
        {
            return new GrammarNoun(text, false);
        }
    }
}
