﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;

namespace Recurse.Azar.Language.Verbs
{
    public class Pronoun : Dictionary<PronounType, GrammarNoun>, IGrammarObject, IGrammarAlias
    {
        public static readonly List<Pronoun> List = new List<Pronoun>();

        public static readonly Pronoun
            It = new Pronoun(GrammarGender.Neutral, "it", "it", "it", "its", "itself"),
            Male = new Pronoun(GrammarGender.Male, "he", "him", "his", "his", "himself"),
            Female = new Pronoun(GrammarGender.Female, "she", "her", "her", "hers", "herself"),
            Neutral = new Pronoun(GrammarGender.Neutral, "they", "them", "their", "theirs", "themself", true),
            Spivak = new Pronoun(GrammarGender.Neutral, "ey", "em", "eir", "eirs", "eirself"),
            Hir = new Pronoun(GrammarGender.Neutral, "ze", "hir", "hir", "hirs", "hirself"),
            Zir = new Pronoun(GrammarGender.Neutral, "ze", "zir", "zir", "zirs", "zirself"),
            Ne = new Pronoun(GrammarGender.Neutral, "ne", "nem", "nir", "nirs", "nemself"),
            Ve = new Pronoun(GrammarGender.Neutral, "ve", "ver", "vis", "vis", "verself"),
            Xe = new Pronoun(GrammarGender.Neutral, "xe", "xem", "xyr", "xyrs", "xemself");

        #region Instance Members

        public GrammarGender Gender { get; private set; }

        public double Specificity { get { return 0; } }

        public Pronoun(
            GrammarGender gender,
            string they,
            string them, 
            string their,
            string theirs,
            string themself,
            bool isPlural = false)
        {
            Gender = gender;
            this[PronounType.They] = new GrammarNoun(they, isPlural);
            this[PronounType.Them] = new GrammarNoun(them, isPlural);
            this[PronounType.Their] = new GrammarNoun(their, isPlural);
            this[PronounType.Theirs] = new GrammarNoun(theirs, isPlural);
            this[PronounType.Themself] = new GrammarNoun(themself, isPlural);

            List.Add(this);
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            return this[type];
        }

        #endregion
    }
}
