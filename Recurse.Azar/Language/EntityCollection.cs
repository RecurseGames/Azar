﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using System.Collections.Generic;
using System.Linq;
using Recurse.Azar.Model;
using Recurse.Azar.Language.Verbs;
using System;
using Recurse.Azar.Model.Text;

namespace Recurse.Azar.Language
{
    public class EntityCollection : IGrammarObject, IEquatable<EntityCollection>
    {
        private readonly Entity[] _contents;

        private readonly TextContext _context;

        private readonly AliasMapping _aliases;

        public static IGrammarObject TryCreate(IEnumerable<Entity> contents, TextContext context)
        {
            return TryCreate(contents, context, null);
        }

        public static IGrammarObject TryCreate(IEnumerable<Entity> contents, AliasMapping aliases)
        {
            return TryCreate(contents, null, aliases);
        }

        private static IGrammarObject TryCreate(IEnumerable<Entity> contents, TextContext context, AliasMapping aliases)
        {
            if (contents == null) return null;
            var contentArray = contents.ToArray();
            if (contentArray.Length == 0) return null;
            if (contentArray.Length == 1)
                return context != null ? contentArray[0].GetReference(context) : contentArray[0].GetSimpleReference(aliases);
            return new EntityCollection(contents, context, aliases);
        }

        public EntityCollection(IEnumerable<Entity> contents, TextContext context, AliasMapping aliases)
        {
            _aliases = aliases;
            _context = context;
            if (contents == null)
                throw new ArgumentNullException("contents");
            _contents = contents.ToArray();
            if (_contents.Count() <= 1)
                throw new ArgumentException("contents", "contents must contain more than one element");
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            if (_context != null)
            {
                var pronoun = _context.Aliases.Get(this);
                if (pronoun != null) return pronoun.GetNoun(type);
                _context.Aliases.Add(Pronoun.Neutral, this);
            }

            var delimitedString = new DelimitedString
            {
                Delimiter = RenderSymbol.Comma,
                FinalDelimiter = "and"
            };
            foreach (var atom in _contents)
            {
                delimitedString.Symbols.Add(_context != null ?
                    atom.GetReference(_context).GetNoun(type).Hyperstring :
                    atom.GetSimpleReference(_aliases).GetNoun(type).Hyperstring);
            }
            return new GrammarNoun(new Hyperstring.Simple(delimitedString), true);
        }

        public bool Equals(EntityCollection other)
        {
            return 
                other != null &&
                _contents.Intersect(other._contents).Count() == _contents.Length;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as EntityCollection);
        }

        public override int GetHashCode()
        {
            var result = 0;
            foreach (var item in _contents) result += item.GetHashCode();
            return result;
        }
    }
}
