﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language.Verbs;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Language
{
    public class AliasMapping
    {
        private readonly Dictionary<IGrammarAlias, object> _mappings = 
            new Dictionary<IGrammarAlias, object>();

        public IEnumerable<object> Targets
        {
            get { return _mappings.Values; }
        }
        
        public IGrammarObject Get(object target)
        {
            return _mappings.OrderBy(e => e.Key.Specificity).LastOrDefault(e => Equals(e.Value, target)).Key;
        }
        
        public object GetTarget(IGrammarAlias alias)
        {
            return _mappings.ContainsKey(alias) ? _mappings[alias] : null;
        }

        public void Add(IGrammarAlias pronoun, object target)
        {
            if (!_mappings.ContainsKey(pronoun))
                _mappings.Add(pronoun, target);
            else if (!Equals(_mappings[pronoun], target))
                _mappings[pronoun] = null;
        }

        public void Clear()
        {
            _mappings.Clear();
        }

        public void Remove(IEnumerable<IGrammarAlias> aliases)
        {
            foreach (var alias in aliases)
                _mappings.Remove(alias);
        }

        public void ClearAmbiguities()
        {
            foreach (var ambiguity in _mappings.Where(e => e.Value == null).ToArray())
                _mappings.Remove(ambiguity.Key);
        }
    }
}
