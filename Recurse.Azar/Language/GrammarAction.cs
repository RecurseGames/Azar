﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using System;

namespace Recurse.Azar.Language
{
    public abstract class GrammarAction
    {
        public abstract bool? RequiredTransitivity { get; }

        public GrammarAction Inverse { get; private set; }

        public Hyperstring ToSequence(
            IGrammarObject topic,
            IGrammarObject focus,
            GrammarCategory category)
        {
            if (RequiredTransitivity == true && focus == null)
                throw new ArgumentException("Transitive action requires focus.");
            if (RequiredTransitivity == false && focus != null)
                throw new ArgumentException("Intransitive action cannot receive focus.");
            var result = GetSequence(topic, focus, category);
            if (result == null)
                throw new NotImplementedException("Category not implemented: " + category);
            return result;
        }

        protected void SetInverse(GrammarAction inverse)
        {
            if (Inverse != null)
                throw new InvalidOperationException("Inverse already set");
            if (inverse.Inverse != null)
                throw new InvalidOperationException("Inverse already has inverse");
            Inverse = inverse;
            inverse.Inverse = this;
        }

        protected abstract Hyperstring GetSequence(
            IGrammarObject topic,
            IGrammarObject focus,
            GrammarCategory category);
    }
}
