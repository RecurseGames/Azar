﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class BetrayVerb : ComplexGrammarAction
    {
        public override bool? RequiredTransitivity
        {
            get { return true; }
        }

        protected override Hyperstring GetVerb(GrammarCategory category, bool topicIsAgent, bool isTransitive)
        {
            switch (category)
            {
                case GrammarCategory.SimplePast:
                    return topicIsAgent ? "betrayed" : "was betrayed by";
                case GrammarCategory.SimplePastFuture:
                    return topicIsAgent ? "would betray" : "would be betrayed by";
                default:
                    return null;
            }
        }
    }
}
