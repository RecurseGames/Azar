﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class IsAdjective : GrammarAction
    {
        private readonly string _adjective;

        public override bool? RequiredTransitivity
        {
            get { return false; }
        }

        public IsAdjective(string adjective)
        {
            _adjective = adjective;
        }

        protected override Hyperstring GetSequence(
            IGrammarObject topic, 
            IGrammarObject focus, 
            GrammarCategory category)
        {
            var topicNoun = topic.GetNoun(PronounType.They);
            var verb = Equals(category, GrammarCategory.SimplePast) ?
                topicNoun.IsPlural ? "were" : "was" :
                "would be";
            var result =
                Hyperstring.Empty +
                topicNoun.Hyperstring +
                verb +
                _adjective;
            return result;
        }
    }
}
