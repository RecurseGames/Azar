﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;

namespace Recurse.Azar.Language.Verbs
{
    public class IsMarriedVerb : GrammarAction
    {
        public override bool? RequiredTransitivity
        {
            get { return false; }
        }

        protected override Hyperstring GetSequence(
            IGrammarObject topic,
            IGrammarObject focus,
            GrammarCategory category)
        {
            if (!Equals(category, GrammarCategory.SimplePast))
                return topic.GetNoun(PronounType.They).Hyperstring + "would be married";

            var topicNoun = topic.GetNoun(PronounType.They);
            return
                topicNoun.Hyperstring +
                (topicNoun.IsPlural  ? "were married" : "was married");
        }
    }
}
