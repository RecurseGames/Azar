﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using System;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class IsVerb : SimpleGrammarAction
    {
        public override bool? RequiredTransitivity
        {
            get { return false; }
        }

        protected override Hyperstring GetVerb(GrammarCategory category, bool isTransitive)
        {
            switch (category)
            {
                case GrammarCategory.SimplePast:
                    return "was";
                case GrammarCategory.SimplePastFuture:
                    return "would be";
                default:
                    return null;
            }
        }
    }
}
