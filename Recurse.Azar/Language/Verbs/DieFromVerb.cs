﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Locations;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class DieFromVerb : ComplexGrammarAction
    {
        public override bool? RequiredTransitivity
        {
            get { return true; }
        }

        protected override Hyperstring GetVerb(GrammarCategory category, bool topicIsAgent, bool isTransitive)
        {
            switch (category)
            {
                case GrammarCategory.SimplePast:
                    return topicIsAgent ? "died from" : "killed";
                case GrammarCategory.SimplePastFuture:
                    return topicIsAgent ? "would die from" : "would kill";
                default:
                    return null;
            }
        }
    }
}
