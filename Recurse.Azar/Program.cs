﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Dependencies;
using Recurse.Common.Extensions;
using Recurse.Common.Maths.Space.Areas;
using Recurse.Common.Maths.Space.Points;
using Recurse.Common.Scheduling;
using Recurse.Common.Scheduling.Events;
using Recurse.Common.Text;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Chapters;
using Recurse.Azar.Model.Hatred;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Mortality;
using Recurse.Azar.Model.Occupations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Events;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Politics;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Residency;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Sex.Reproduction;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Model.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar
{
    public class Program
    {
        public static readonly Random Rng = new Random(1);

        public static void Main(string[] args)
        {
            var startTime = default(Time<Duration>) + Durations.Year * 100;
            var cosmos = new Cosmos(Rng)
            {
                Time = startTime
            };
            cosmos.Tracker.StartTracking(CauseBirth.All);
            cosmos.Tracker.StartTracking(Plague.Notable);
            cosmos.Tracker.StartTracking(ConsiderEmigration.TotalExecuted);
            cosmos.Tracker.StartTracking(ConsiderEmigration.TotalExecutedSizes);
            cosmos.Tracker.StartTracking(StartEmnity.Started);
            cosmos.Tracker.StartTracking(Feud.List);

            const int tilesSqrt = 3;
            var grid = new Grid(cosmos, new Rect<int>(tilesSqrt, tilesSqrt));

            var locations = grid.Area.GetPoints().Select(pt => grid[pt]);
            var philosophies = CauseBirth.All.Get(cosmos.Tracker)
                .SelectMany(p => p.Attributes.OfType<HasPhilosophy>())
                .Select(attr => attr.Philosophy)
                .Distinct();
            var people = CauseBirth.All.Get(cosmos.Tracker);

            var eventCount = 0;
            var start = DateTime.Now;
            var lastUpdate = DateTime.Now;
            while (
                (!Console.KeyAvailable || Console.ReadKey().KeyChar != 's') &&
                cosmos.Events.Time.HasValue)
            {
                cosmos.Time = cosmos.Events.Time.Value;
                cosmos.Events.Occur();
                eventCount++;

                if ((DateTime.Now - lastUpdate).TotalSeconds > 1 / 6d)
                {
                    lastUpdate = DateTime.Now;
                    Console.Clear();
                    Console.WriteLine(string.Format(
                        "Year: " + cosmos.Time.GetDifference(default(Time<Duration>)) / Durations.Year));
                    Console.WriteLine(string.Format(
                        "Events: " + eventCount));
                    var elapsed = (DateTime.Now - start).TotalMilliseconds;
                    var yearsElapsed = cosmos.Time.GetDifference(startTime) / Durations.Year;
                    Console.WriteLine(string.Format(
                        "Year execution time (ms): {0}",
                        elapsed / yearsElapsed));

                    //Console.WriteLine("Population");
                    //var livingPeople = people.Where(p => !p.IsDead);
                    //Console.WriteLine("  Living: " +
                    //    livingPeople.Count());
                    //var livingAdults = livingPeople.Where(p => Age.GetYears(p, cosmos.Time) > 18);
                    //Console.WriteLine("    Adults: " +
                    //    livingAdults.Count());
                    //Console.WriteLine("      Single: " +
                    //    livingAdults.Count(p => !p.Attributes.OfType<HasRomance>().Any()));
                    //Console.WriteLine("  Nonbinary: " +
                    //    people.Count(p => Equals(p.Sexuality.Pronouns.Gender, GrammarGender.Neutral)));
                    //Console.WriteLine("  Total: " +
                    //    people.Count());
                    
                    //var livingPhilosophies = philosophies
                    //    .Where(p => p.Adherents.Any(a => !a.IsDead))
                    //    .ToArray();
                    //Console.WriteLine(string.Format(
                    //    "Active philosophies: {0} ({1} total)",
                    //    livingPhilosophies.Count(),
                    //    philosophies.Count()));
                    //var adherentCounts = livingPhilosophies
                    //    .Select(p => p.Adherents.Count(a => !a.IsDead))
                    //    .DefaultIfEmpty()
                    //    .ToArray();
                    //var averageAdherents = adherentCounts.Average();
                    //Console.WriteLine(string.Format(
                    //    "  Living adherents: min {0}, max {1}, avg {2:F2}, sd {3:F2}",
                    //    adherentCounts.Min(),
                    //    adherentCounts.Max(),
                    //    adherentCounts.Average(),
                    //    adherentCounts.Select(span => Math.Abs(averageAdherents - span)).Average()));
                    //var pLocationCounts = livingPhilosophies
                    //    .Select(p => p.Adherents.Select(a => a.Location.Get()).Distinct().Count())
                    //    .DefaultIfEmpty();
                    //Console.WriteLine(string.Format(
                    //    "  Locations covered: min {0}, max {1}, avg {2:F2}, sd {3:F2}",
                    //    pLocationCounts.Min(),
                    //    pLocationCounts.Max(),
                    //    pLocationCounts.Average(),
                    //    pLocationCounts.Select(span => Math.Abs(pLocationCounts.Average() - span)).Average()));

                    //var migrationSizes = ConsiderEmigration.TotalExecutedSizes.Get(cosmos.Tracker).DefaultIfEmpty();
                    //Console.WriteLine(string.Format(
                    //    "Migration Sizes: {0:F2} avg, {1:F2} max",
                    //    migrationSizes.Average(),
                    //    migrationSizes.Max()));
                    //var migrants = people.Where(p => p.Records.OfType<ResidenceRecord>().Count() > 1);
                    //Console.WriteLine("Migrants: " + migrants.Count());
                    //var migrationsPerPerson = migrants.Select(m => m.Records.OfType<ResidenceRecord>().Count()).DefaultIfEmpty();
                    //Console.WriteLine(string.Format(
                    //    "Migrations per person: {0:F2} avg, {1:F2} max",
                    //    migrationsPerPerson.Average(),
                    //    migrationsPerPerson.Max()));

                    //Console.WriteLine("Occupations");
                    //var occupations = people.SelectMany(p => p.Attributes.OfType<HasOccupation>());
                    //foreach (var job in occupations.Select(o => o.Occupation).GroupBy(o => o))
                    //    Console.WriteLine("  " + job.Key + ": " + job.Count().ToString());
                    //Console.WriteLine(string.Format(
                    //    "  Avg: {0:F2}, Max: {1:F2}",
                    //    occupations.Select(o => o.Number).DefaultIfEmpty().Average(),
                    //    occupations.Select(o => o.Number).DefaultIfEmpty().Max()));

                    //Console.WriteLine("Deaths");
                    //var deaths = people.SelectMany(p => p.Attributes.OfType<IsDead>());
                    //foreach (var cause in deaths.Select(d => d.TypeOfDeath).GroupBy(d => d))
                    //    Console.WriteLine("  " + cause.Key + ": " + cause.Count().ToString());
                    //var deadPeople = people.Where(p => p.Attributes.OfType<IsDead>().Any());
                    //var deathAges = deadPeople
                    //    .Select(p => Age.GetYears(p, p.Attributes.OfType<IsDead>().Single().TimeOfDeath))
                    //    .DefaultIfEmpty();
                    //var averageAge = deathAges.Average();
                    //Console.WriteLine(string.Format(
                    //    "  Lifespans: min {0:F2}, max {1:F2}, avg {2:F2}, sd {3:F2}",
                    //    deathAges.Min(),
                    //    deathAges.Max(),
                    //    deathAges.Average(),
                    //    deathAges.Select(span => Math.Abs(averageAge - span)).Average()));

                    //{
                    //    Console.WriteLine("Murderers");
                    //    var murderers2 = people.SelectMany(p => p.Attributes.OfType<IsMurderer>()).ToList();
                    //    Console.WriteLine("  Total: " +
                    //        murderers2.Count());
                    //    Console.WriteLine("  Average victims: " +
                    //        murderers2.Select(m => m.Victims.Count).DefaultIfEmpty().Average());
                    //    Console.WriteLine("  Max victims: " +
                    //        murderers2.Select(m => m.Victims.Count).DefaultIfEmpty().Max());
                    //}
                    
                    //Console.WriteLine("Bloodlines");
                    //var bloodlines = people
                    //    .SelectMany(p => p.Attributes.OfType<IsInBloodline>())
                    //    .Select(a => a.Bloodline)
                    //    .Distinct()
                    //    .ToArray();
                    //Console.WriteLine(string.Format(
                    //    "  Longest: {0}",
                    //    bloodlines.Select(b => b.HighestGeneration).DefaultIfEmpty().Max()));
                    //Console.WriteLine(string.Format(
                    //    "  Longest living: {0}",
                    //    livingPeople.SelectMany(p => p.Attributes.OfType<IsInBloodline>()).Select(b => b.Generation).DefaultIfEmpty().Max()));

                    //Console.WriteLine("Notable Plagues");
                    //Console.WriteLine(string.Format(
                    //    "  Total: {0}", Plague.Notable.Get(cosmos.Tracker).Count()));
                    //var victimCounts = Plague.Notable.Get(cosmos.Tracker).Select(p => p.Victims.Count()).DefaultIfEmpty();
                    //Console.WriteLine(string.Format(
                    //    "  Average victims: {0}",
                    //    victimCounts.Average()));
                    //Console.WriteLine(string.Format(
                    //    "  Max victims: {0}",
                    //    victimCounts.Max()));
                    //var locationCounts = Plague.Notable.Get(cosmos.Tracker).Select(p => p.Locations.Count()).DefaultIfEmpty();
                    //Console.WriteLine(string.Format(
                    //    "  Average locations: {0}",
                    //    locationCounts.Average()));
                    //Console.WriteLine(string.Format(
                    //    "  Max locations: {0}",
                    //    locationCounts.Max()));

                    //Console.WriteLine("Locations with living populations");
                    //var livingLocations = locations
                    //    .Where(l => l.Contents.OfType<Person>().Any(p => !p.IsDead))
                    //    .ToList();
                    //Console.WriteLine(string.Format(
                    //    "  Total: {0}",
                    //    livingLocations.Count));
                    //var livingPopulations = livingLocations
                    //    .Select(l => l.Contents.OfType<Person>().Count(p => !p.IsDead))
                    //    .ToList();
                    //Console.WriteLine(string.Format(
                    //    "  Average population: {0}",
                    //    livingPopulations.Average()));
                    //Console.WriteLine(string.Format(
                    //    "  Min population: {0}",
                    //    livingPopulations.Min()));
                    //Console.WriteLine(string.Format(
                    //    "  Max population: {0}",
                    //    livingPopulations.Max()));

                    Console.WriteLine("Press 's' to stop.");
                }
            }

            Console.WriteLine();

            // Example #3: Write only some strings in an array to a file.
            // The using statement automatically flushes AND CLOSES the stream and calls 
            // IDisposable.Dispose on the stream object.
            // NOTE: do not use FileStream for text files because it writes bytes, but StreamWriter
            // encodes the output as text.

            var monarchies = people.SelectMany(p => p.Attributes.OfType<HasBeenMonarch>()).Select(a => a.Target).Distinct();
            var bloodlinez = people
                        .SelectMany(p => p.Attributes.OfType<IsInBloodline>())
                        .Select(a => a.Bloodline)
                        .Distinct()
                        .ToArray();

            var feuds = Feud.List.Get(cosmos.Tracker).ToList();

            var indexEntries = new Dictionary<string, IEnumerable<Entity>>
            {
                { "People", people.Cast<Entity>() },
                { "Rebels", people.Where(p => p.Attributes.Contains(Model.Attribute.IsRebel)).Cast<Entity>() },
                { "Places", locations.Cast<Entity>() },
                { "States", monarchies.Cast<Entity>() },
                { "Bloodlines", bloodlinez.Cast<Entity>() },
                { "Plagues", Plague.Notable.Get(cosmos.Tracker).Cast<Entity>() },
                { "Religions", philosophies.Cast<Entity>() },
                { "Feuds", feuds.Cast<Entity>() }
            };

            var renderer = new HtmlTextRenderer();
            var murderers = people
                .Where(p => p.Attributes.OfType<IsMurderer>().Any())
                .OrderByDescending(p => p.Attributes.OfType<IsMurderer>().Single().Victims.Count)
                .ToList();
            OutputIndex(renderer, indexEntries, new Dictionary<string, Entity>
            {
                { "Largest bloodline", bloodlinez.OrderBy(b => b.Members.Count).LastOrDefault() },
                { "Largest living bloodline", bloodlinez.Where(b => b.Members.Any(m => !m.IsDead)).OrderBy(b => b.Members.Count).LastOrDefault() },
                { "Longest bloodline", bloodlinez.OrderBy(b => b.Members.Max(m => m.Attributes.OfType<IsInBloodline>().Single().Generation)).LastOrDefault() },
                { "Longest living bloodline", bloodlinez.Where(b => b.Members.Any(m => !m.IsDead)).OrderBy(b => b.Members.Max(m => m.Attributes.OfType<IsInBloodline>().Single().Generation)).LastOrDefault() },
                { "Longest monarchy", monarchies.OrderBy(m => m.TotalMonarchs).LastOrDefault() },
                { "Longest living monarchy", monarchies.OrderBy(m => m.TotalMonarchs).Where(m => m.Monarch != null).LastOrDefault() },
                { "Most prolific murderer", murderers.FirstOrDefault() },
                { "Oldest person", people.Where(p => p.IsDead).OrderByDescending(p => Age.GetYears(p, p.Attributes.OfType<IsDead>().Single().TimeOfDeath)).FirstOrDefault() }
            });

            var entities = indexEntries.SelectMany(e => e.Value).ToArray();
            
            var records = entities
                .SelectMany(e => e.Records.OfType<IRecord>())
                .Distinct()
                .OrderBy(r => r.Time)
                .ToArray();
            using (StreamWriter file = new StreamWriter("dump.html"))
            {
                file.WriteLine(Header);
                file.WriteLine("<h1>Renderings</h1>");
                for (var i = 0; i < records.Length; i++)
                {
                    var record = records[i];
                    var rendering = new RecordRenderer().TryRender(record);
                    if (rendering != null)
                        file.WriteLine("<p>" + renderer.GetString(rendering) + "</p>");
                    Console.Clear();
                    Console.WriteLine(
                        "Dumping records: " +
                        i.ToString().PadLeft(7) +
                        " / " +
                        records.Length.ToString().PadLeft(7));
                }
                file.WriteLine(Footer);
            }

            for (var i = 0; i < entities.Length; i++)
            {
                var entity = entities[i];
                OutputEntity(entity, renderer);

                Console.Clear();
                Console.WriteLine(
                    "Writing entities: " +
                    i.ToString().PadLeft(5) +
                    " / " +
                    entities.Length.ToString().PadLeft(5));
            }
        }

        private static void OutputEntity(Entity entity, HtmlTextRenderer renderer)
        {
            if (entity == null) return;
            using (StreamWriter file = new StreamWriter(renderer.GetId(entity) + ".html"))
            {
                file.WriteLine(Header);
                file.WriteLine(string.Format(
                    "<h1>{0}</h1>",
                    renderer.GetString(entity.GetReference(new TextContext()).GetNoun(PronounType.They).Hyperstring)));

                file.WriteLine("<h2>Abridged Biography</h2>");

                var chapter = new Biography(entity);
                file.WriteLine(string.Format(
                    "<div>{0}</div>",
                    renderer.GetString(chapter.ToSequence())));

                file.WriteLine("<h2>Unabridged Biography</h2>");

                var detailedChapter = new Biography(entity, true);
                file.WriteLine(string.Format(
                    "<div>{0}</div>",
                    renderer.GetString(detailedChapter.ToSequence())));

                file.WriteLine(Footer);
            }
        }

        private static void OutputIndex(
            HtmlTextRenderer renderer,
            Dictionary<string, IEnumerable<Entity>> indexEntries,
            Dictionary<string, Entity> entries)
        {
            using (StreamWriter file = new StreamWriter("output.html"))
            {
                file.WriteLine(Header);
                file.WriteLine("<h1>Contents</h1>");
                file.WriteLine("<h2>Notable Figures</h2>");
                foreach (var entry in entries)
                {
                    var entity = entry.Value;
                    file.WriteLine(string.Format(
                        "<p>{0}: <a href='{1}'>{2}</a></p>",
                        entry.Key,
                        entity == null ? "#" : (renderer.GetId(entry.Value) + ".html"),
                        entity == null ? "null" : renderer.GetString(entity.GetReference().GetNoun(PronounType.They).Hyperstring)));
                }
                file.WriteLine("<h2>Index</h2>");
                file.WriteLine("<p><a href='dump.html'>Record Index</a></p>");
                foreach (var e in indexEntries)
                {
                    file.WriteLine("<h3>" + e.Key + "</h3>");
                    foreach (var entity in e.Value)
                    {
                        file.WriteLine(string.Format(
                            "<p><a href='{0}.html'>{1}</a></p>",
                            renderer.GetId(entity),
                            renderer.GetString(RenderTitle(entity))));
                    }
                }
                file.WriteLine(Footer);
            }
        }

        private static Hyperstring RenderTitle(Entity entity)
        {
            var result = entity.GetReference().GetNoun(PronounType.They).Hyperstring;

            var bloodline = entity as Bloodline;
            if (bloodline != null)
            {
                result += string.Format(
                    " ({0} generations, {1} members)",
                    bloodline.Members.Max(m => m.Attributes.OfType<IsInBloodline>().Single().Generation),
                    bloodline.Members.Count);
            }

            var monarchy = entity as Monarchy;
            if (monarchy != null)
            {
                result += string.Format(
                    " ({0} monarchs)",
                    monarchy.TotalMonarchs);
            }

            var plague = entity as Plague;
            if (plague != null)
            {
                result += string.Format(
                    " ({0} victims)",
                    plague.Victims.Count());
            }

            var feud = entity as Feud;
            if (feud != null)
            {
                result += string.Format(
                    " ({0} records)",
                    feud.Records.OfType<IRecord>().Count());
            }

            return result;
        }

        private const string
            Header = @"<!DOCTYPE HTML><html lang='en'><head>
<title>Lives of the Azar</title>
<link href='https://fonts.googleapis.com/css?family=Fondamento' rel='stylesheet'>
<style type='text/css'>
body { color: #041027; background: #ebd5b3; width: 90%; margin: 1em auto; font-family: 'Fondamento', cursive; text-align: center; font-size: 120%; }
a { color: #ab0010; text-decoration: none; }
a:hover { color: #d50013; }
</style></head><body>
<a href='output.html'>Lives of the Azar</a>",
            Footer = "</body></html>";
    }
}
