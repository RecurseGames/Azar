﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Tracking;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model
{
    public class Tracker
    {
        private readonly Dictionary<ITrackingCategory, object> _data = 
            new Dictionary<ITrackingCategory, object>();

        private readonly List<ITrackingCategory> _categories = 
            new List<ITrackingCategory>();

        public void StartTracking(ITrackingCategory category)
        {
            if (_data.ContainsKey(category))
                throw new InvalidOperationException("Already tracking category.");
            _data[category] = category.CreateData();
            _categories.Add(category);
        }

        public void EnsureTracking(ITrackingCategory category)
        {
            if (!_data.ContainsKey(category))
            {
                _data[category] = category.CreateData();
                _categories.Add(category);
            }
        }

        public object GetData(ITrackingCategory category)
        {
            if (!_data.ContainsKey(category))
                throw new InvalidOperationException("Category not tracked.");
            return _data[category];
        }

        public bool TryUpdate(ITrackingCategory category, Func<object, object> update)
        {
            if (!_data.ContainsKey(category))
                return false;
            _data[category] = update(_data[category]);
            return true;
        }

        public bool TryUpdate(ITrackingCategory category, Action<object> update)
        {
            if (!_data.ContainsKey(category))
                return false;
            update(_data[category]);
            return true;
        }
    }
}
