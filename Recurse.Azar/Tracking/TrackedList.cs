﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model;
using System.Collections.Generic;

namespace Recurse.Azar.Tracking
{
    public class TrackedList<T> : TrackingCategory<List<T>>
    {
        private readonly string _name;

        public TrackedList(string name)
        {
            _name = name;
        }

        public IEnumerable<T> Get(Tracker tracker)
        {
            return GetData(tracker);
        }

        public void TryAdd(Tracker tracker, T item)
        {
            TryUpdate(tracker, list => list.Add(item));
        }

        protected override List<T> CreateData()
        {
            return new List<T>();
        }
    }
}
