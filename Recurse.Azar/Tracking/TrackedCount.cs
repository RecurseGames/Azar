﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model;

namespace Recurse.Azar.Tracking
{
    public class TrackedCount : TrackingCategory<int>
    {
        private readonly string _name;

        public TrackedCount(string name)
        {
            _name = name;
        }

        public int Get(Tracker tracker)
        {
            return GetData(tracker);
        }

        public void TryAdd(Tracker tracker, int value = 1)
        {
            TryUpdate(tracker, data => data + value);
        }

        protected override int CreateData()
        {
            return 0;
        }
    }
}
