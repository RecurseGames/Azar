﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model;
using System;

namespace Recurse.Azar.Tracking
{
    public abstract class TrackingCategory<TData> : ITrackingCategory
    {
        protected abstract TData CreateData();

        protected TData GetData(Tracker tracker)
        {
            return (TData)tracker.GetData(this);
        }

        protected bool TryUpdate(Tracker tracker, Action<TData> update)
        {
            return tracker.TryUpdate(this, data => update((TData)data));
        }

        protected bool TryUpdate(Tracker tracker, Func<TData, TData> update)
        {
            return tracker.TryUpdate(this, data => update((TData)data));
        }

        object ITrackingCategory.CreateData()
        {
            return CreateData();
        }
    }
}
