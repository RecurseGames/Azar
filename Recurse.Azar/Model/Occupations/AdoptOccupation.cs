﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Maths;
using Recurse.Common.Scheduling.Events;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using RG.Saga.Model.Timing.Randomised;
using System.Linq;
using System.Collections.Generic;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Language.Verbs;
using Recurse.Common.Text;
using Recurse.Azar.Model.Text;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Occupations
{
    public class AdoptOccupation : MutableEvent<Time<Duration>?>
    {
        private readonly Cosmos _cosmos;

        private readonly Person _person;

        public AdoptOccupation(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;

            UpdateTime();
        }

        public override void Occur()
        {   
            var hasOccupation = HasOccupation.Get(_person);
            var adopted = GetNewOccupation();
            if (hasOccupation != null)
            {
                hasOccupation.Update(adopted.Occupation);
            }
            else
            {
                hasOccupation = new HasOccupation(adopted.Occupation);
                _person.Attributes.Add(hasOccupation);
            }

            var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<BecomeVerb>.Instance)
            {
                Subjects = { _person },
                Objects = { adopted }
            };
            _person.Records.Add(record);

            UpdateTime();
        }

        private AdoptedOccupation GetNewOccupation()
        {
            var hasOccupation = HasOccupation.Get(_person);
            var existingOccupation = hasOccupation != null ? hasOccupation.Occupation : null;

            // First occupation is usually inherited
            if (existingOccupation == null)
            {
                var inheritableOccupations = GetInheritableOccupations()
                    .Where(o => !Equals(existingOccupation, o))
                    .ToList();
                if (inheritableOccupations.Any() && _cosmos.Rng.NextDouble() < .9)
                {
                    return _cosmos.Rng.Select(inheritableOccupations);
                }
            }

            // Otherwise, pick at random
            var age = Age.Get(_person, _cosmos.Time);
            var occupations = new WeightedOutcomes<Occupation>(
                Occupation.List,
                o => Equals(o, existingOccupation) || o.MaxAdoptionAge.HasValue && o.MaxAdoptionAge.Value.CompareTo(age) < 0 ? 0 : o.RelativeFrequency);
            return new AdoptedOccupation(_person, occupations.GetOutcome(_cosmos.Rng));
        }

        private IEnumerable<AdoptedOccupation> GetInheritableOccupations()
        {
            return ParentageRelationship
                .GetParents(_person)
                .ToDictionary(parent => parent, parent =>
                {
                    var hasOccupation = HasOccupation.Get(parent);
                    if (hasOccupation != null && _cosmos.Rng.NextDouble() < hasOccupation.Occupation.InheritanceChance)
                    {
                        return hasOccupation.Occupation;
                    }
                    return null;
                })
                .Where(e => e.Value != null)
                .Select(e => new AdoptedOccupation(_person, e.Value, e.Key));
        }

        private void UpdateTime()
        {
            var hasOccupation = _person.Attributes.OfType<HasOccupation>().SingleOrDefault();
            if (hasOccupation == null)
            {
                var birth = _person.Attributes.OfType<IsBorn>().Single();
                Time = birth.TimeOfBirth + (16 + _cosmos.Rng.NextDouble() * 2) * Durations.Year;
            }
            else
            {
                Time = _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, .3) * 100 * Durations.Year;
            }
        }

        private class AdoptedOccupation : Entity
        {
            public readonly Person Inspiration;

            private readonly Person _adoptee;

            public readonly Occupation Occupation;

            public AdoptedOccupation(Person adoptee, Occupation occupation, Person inspiration = null)
            {
                _adoptee = adoptee;
                Occupation = occupation;
                Inspiration = inspiration;
            }

            protected override IGrammarObject CreateReference(TextContext context)
            {
                if (Inspiration == null)
                    return base.CreateReference(context);
                return new TitledGrammarObject(
                    new TempEntity("a " + Occupation.Name + " like"),
                    Inspiration.GetReference(context));
            }

            public override IEnumerable<Entity> GetRelatives()
            {
                yield break;
            }

            public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
            {
                throw new InvalidOperationException();
            }

            public override IGrammarObject GetSimpleReference(AliasMapping aliases)
            {
                return new TempEntity("a " + Occupation.Name);
            }

            public override IEnumerable<IRecord> GetIntroduction(TextContext context)
            {
                yield break;
            }

            public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
            {
                yield break;
            }
        }
    }
}
