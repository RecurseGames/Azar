﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language;
using System.Collections.Generic;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Locations;
using Recurse.Ianna.Model;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Occupations
{
    public class Occupation
    {
        public static readonly List<Occupation> List = new List<Occupation>();

        public static readonly Occupation
            // Farmer's often create buildings or textiles, or serve as fighters.
            Farmer = new Occupation
            {
                Name = "farmer",
                RelativeFrequency = 1,
                RelativeStartPhilosophyChance = .01
            },
            // Warriors are often co-opted into law enforcement activities
            Warrior = new Occupation
            {
                Name = "warrior",
                RelativeFrequency = 0.25,
                MaxAdoptionAge = 45 * Durations.Year,
                RelativeStartPhilosophyChance = .01
            },
            Merchant = new Occupation
            {
                Name = "merchant",
                RelativeFrequency = 0.25,
                MaxAdoptionAge = 45 * Durations.Year
            },
            Smith = new Occupation
            {
                Name = "blacksmith",
                InheritanceChance = .5,
                RelativeFrequency = 0.15
            },
            Criminal = new Occupation
            {
                Name = "criminal",
                InheritanceChance = .8,
                RelativeFrequency = 0.2,
                RelativeStartPhilosophyChance = .01
            },
            Poet = new Occupation
            {
                Name = "poet",
                InheritanceChance = .5,
                RelativeFrequency = 0.1,
                RelativeStartPhilosophyChance = .01
            },
            // Special mystic class?
            Mystic = new Occupation
            {
                Name = "mystic",
                InheritanceChance = .05,
                RelativeFrequency = 0.05,
                RelativeStartPhilosophyChance = .3
            };

        public string Name { get; set; }

        public double InheritanceChance { get; set; }

        public double RelativeFrequency { get; set; }

        public double RelativeStartPhilosophyChance { get; set; }

        public Measurement<Duration>? MaxAdoptionAge { get; set; }

        public Occupation()
        {
            InheritanceChance = 1;
            RelativeFrequency = 1;

            List.Add(this);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
