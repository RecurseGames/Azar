﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Linq;

namespace Recurse.Azar.Model.Occupations
{
    public class HasOccupation : IAttribute
    {
        public Occupation Occupation { get; private set; }

        public int Number { get; private set; }

        public HasOccupation(Occupation occupation)
        {
            Occupation = occupation;
            Number = 1;
        }

        public void Update(Occupation occupation)
        {
            if (Equals(occupation, Occupation))
                throw new ArgumentException("Aready have that occupation");
            Number++;
            Occupation = occupation;
        }

        public static HasOccupation Get(Entity entity)
        {
            return entity.Attributes.OfType<HasOccupation>().SingleOrDefault();
        }
    }
}
