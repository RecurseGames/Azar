﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model
{
    public class SyllableList : List<Syllable>
    {
        public Syllable[] CreateString(Random rng, int length, int attempts = 10000)
        {
            while (attempts-- > 0)
            {
                var result = TryCreateString(rng, length);
                if (result != null)
                    return result;
            }
            return null;
        }

        public Syllable[] TryCreateString(Random rng, int length)
        {
            if (length < 0)
                throw new ArgumentOutOfRangeException("length", "Length must be 0 or greater");

            var result = new List<Syllable>();
            for (var i = 0; i < length; i++)
            {
                if (!TryAdd(rng, result, i == length - 1))
                    return null;
            }
            return result.ToArray();
        }
        
        public bool TryAdd(Random rng, List<Syllable> list, bool isTerminal)
        {
            var previous = list.LastOrDefault();
            var candidates = this
                .Where(s => s.CanFollow(previous) && (!isTerminal || s.CanEnd))
                .ToArray();
            if (!candidates.Any()) return false;
            list.Add(rng.Select(candidates));
            return true;
        }
    }
}
