﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Azar.Model
{
    public class SyllableEnd
    {
        public static readonly SyllableEnd
            Vowel = new SyllableEnd("Vowel"),
            Consonant = new SyllableEnd("Consonant"),
            Undefined = new SyllableEnd("Undefined");

        public static readonly SyllableEnd[] AllTypes = new[] { null, Vowel, Consonant };

        public static readonly SyllableEnd[] NotConsonants = new[] { null, Vowel };

        public static readonly SyllableEnd[] NotNull = new[] { Consonant, Vowel };

        public static readonly SyllableEnd[] NotVowel = new[] { Consonant, null };

        private readonly string _string;

        public SyllableEnd(string s)
        {
            _string = s;
        }

        public override string ToString()
        {
            return _string;
        }
    }
}
