﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model
{
    public class Syllable
    {
        private readonly SyllableEnd[] _canFollow, _canPrecede;

        private readonly SyllableEnd _start, _end;

        public readonly string Text;

        public bool CanStart
        {
            get { return _canFollow.Contains(null); }
        }

        public bool CanEnd
        {
            get { return _canPrecede.Contains(null); }
        }

        public Syllable(
            string text,
            SyllableEnd start = null,
            SyllableEnd end = null,
            SyllableEnd[] canFollow = null, 
            SyllableEnd[] canPrecede = null)
        {
            _start = start;
            _end = end;
            _canFollow = canFollow ?? new SyllableEnd[0];
            _canPrecede = canPrecede ?? new SyllableEnd[0];

            Text = text;
        }

        public bool CanFollow(Syllable previous)
        {
            if (previous == null)
                return _canFollow.Contains(null);

            return
                _canFollow.Contains(previous._end) &&
                previous._canPrecede.Contains(_start);
        }

        public static IEnumerable<Syllable> Create(
            SyllableEnd start,
            SyllableEnd end,
            SyllableEnd canFollow,
            SyllableEnd[] canPrecede,
            params string[] syllables)
        {
            return Create(start, end, new[] { canFollow }, canPrecede, syllables);
        }

        public static IEnumerable<Syllable> Create(
            SyllableEnd start,
            SyllableEnd end,
            SyllableEnd canFollow,
            SyllableEnd canPrecede,
            params string[] syllables)
        {
            return Create(start, end, new[] { canFollow }, new[] { canPrecede }, syllables);
        }

        public static IEnumerable<Syllable> Create(
            SyllableEnd start,
            SyllableEnd end,
            SyllableEnd[] canFollow,
            SyllableEnd canPrecede,
            params string[] syllables)
        {
            return Create(start, end, canFollow, new[] { canPrecede }, syllables);
        }

        public static IEnumerable<Syllable> CreateEnd(
            SyllableEnd start,
            SyllableEnd[] canFollow,
            params string[] syllables)
        {
            return Create(start,
                SyllableEnd.Undefined, canFollow, new SyllableEnd[] { null }, syllables);
        }

        public static IEnumerable<Syllable> CreateStart(
            SyllableEnd end,
            SyllableEnd[] canPrecede,
            params string[] syllables)
        {
            return Create(SyllableEnd.Undefined, end, new SyllableEnd[] { null }, canPrecede, syllables);
        }

        public static IEnumerable<Syllable> Create(
            SyllableEnd start,
            SyllableEnd end,
            SyllableEnd[] canFollow,
            SyllableEnd[] canPrecede,
            params string[] syllables)
        {
            return syllables.Select(s => new Syllable(s, start, end, canFollow, canPrecede));
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
