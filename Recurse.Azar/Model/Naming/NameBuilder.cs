﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Azar.Model
{
    public static class NameBuilder
    {
        public static readonly SyllableList Syllables = new SyllableList();

        static NameBuilder()
        {
            Syllables.AddRange(Syllable.Create(
                SyllableEnd.Vowel,
                SyllableEnd.Vowel,
                SyllableEnd.NotVowel,
                SyllableEnd.NotVowel,
                "e", "a", "o", "u", "i", "a", "e", "o", "ia", "ai", "ou", "e", "a", "e", "i"));
            Syllables.AddRange(Syllable.Create(
                SyllableEnd.Consonant,
                SyllableEnd.Consonant,
                SyllableEnd.Vowel,
                SyllableEnd.Vowel,
                "tz", "bz", "lz", "mz", "nz", "z", "ks", "bs", "ds", "gs", "ls", "ps", "km", "bd"));
            Syllables.AddRange(Syllable.Create(
                SyllableEnd.Consonant,
                SyllableEnd.Consonant,
                SyllableEnd.Vowel,
                SyllableEnd.NotConsonants,
                "ld", "nt", "nk", "mb"));
            Syllables.AddRange(Syllable.Create(
                SyllableEnd.Consonant,
                SyllableEnd.Consonant,
                SyllableEnd.NotConsonants,
                SyllableEnd.Vowel,
                "thr", "ts", "kr", "v", "gr", "dr", "br"));
            Syllables.AddRange(Syllable.CreateEnd(
                SyllableEnd.Consonant,
                new[] { SyllableEnd.Vowel },
                "x", "ple", "ble", "dy", "by", "ny", "nk"));
            Syllables.AddRange(Syllable.CreateStart(
                SyllableEnd.Consonant,
                new[] { SyllableEnd.Vowel },
                "pt", "ng", "ts", "kl", "h", "j", "w", "f", "fl"));
            Syllables.AddRange(Syllable.Create(
                SyllableEnd.Consonant,
                SyllableEnd.Consonant,
                SyllableEnd.NotConsonants,
                SyllableEnd.NotConsonants,
                "r", "k", "b", "d", "g", "l", "p", "t", "m", "n", "s", "z",
                "r", "k", "b", "d", "g", "l", "p", "t", "m", "n", "s", "z",
                "th", "sh", "ng", "mb", "ts", "st", "sh", "nk"));
        }
    }
}
