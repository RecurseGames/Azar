﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model
{
    public class AttributeSet : HashSet<IAttribute>
    {
        public T Get<T>(Func<T, bool> predicate = null) where T : IAttribute
        {
            return predicate != null ?
                this.OfType<T>().SingleOrDefault(predicate) :
                this.OfType<T>().SingleOrDefault();
        }

        public void Set<T>(T attribute) where T : IAttribute
        {
            var existing = Get<T>();
            if (existing != null)
                Remove(existing);
            Add(attribute);
        }
    }
}
