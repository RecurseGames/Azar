﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Hashing;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Politics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recurse.Azar.Model.Personalities
{
    public class Trait
    {
        public static readonly List<Trait> List = new List<Trait>();

        public static readonly Trait
            Jealousy = new Trait(
                "forgiving", "teaches forgiveness", "jealousy", "glorifies violence"),
            ClosenessToUnofficialPartners = new Trait(
                "distant", "values unmarried relationships less than married ones",
                "caring", "values unmarried relationships more than married ones"),
            PeacefulExpansion = new Trait(
                "detached", "is skeptical of government authority",
                "judgemental", "calls for the peaceful expansion of government"),
            VassalExpansion = new Trait(
                "poor at delegation", "teaches that success is up to the individual",
                "skilled administrators", "extols the virtues of friendship"),
            ViolentExpansion = new Trait(
                "accomodating", "warns its followers against the evils of war",
                "domineering", "calls its followers to violent conquest"),
            MilitaryMight = new Trait(
                "incompetent", "teaches its followers to divine the future using rune stones",
                "intelligent", "demands its followers train in combat"),
            PoliticalActivity = new Trait(
                "patient", "encourages caution", "politically active", "calls for constant change"),
            EmigrationChance = new Trait(
                "adverse to travel", "revolves around people's attachment to their homeland",
                "unsettled", "encourages constant movement"),
            LikesLongDistanceRomance = new Trait(
                "detached", "calls for the severing of attachments",
                "worldly", "with a poetic tradition of romance over great distance"),
            CommunalParenthood = new Trait(
                "insular", "disapproves of children before marriage",
                "communal", "calls for collective child-rearing"),
            ChildDesire = new Trait(
                "stingy", "disapproves of large families",
                "parental", "calls for its followers to raise large families"),
            FaithWithinMarriage = new Trait(
                "polyamorous", "places low value of monogamy",
                "monogamous", "values monogamy"),
            Chastity = new Trait(
                "promiscuous", "encourages sexual promiscuity",
                "chaste", "teaches chastity"),
            DivorceChance = new Trait(
                "faithful", "is not accepting of divorce",
                "fickle", "is accepting of divorce"),
            Disagreeableness = new Trait(
                "kind", "teaches kindness",
                "disagreeable", "shuns displays of weakness"),
            MarriageChance = new Trait(
                "casual", "places little value on marriage",
                "earnest", "places high value on marriage"),
            BreakUpSpeed = new Trait(
                "indecisive", "places high value on persistence",
                "decisive", "places high value on decisiveness"),
            MarriageSpeed = new Trait(
                "cautious", "which cautions its followers against haste",
                "hasty", "considers hesitation a sign of weakness"),
            Violence = new Trait(
                "peaceful", "exhorts its follows to lead peaceful lives",
                "violent", "demands offences against one's honour to be repaid with blood"),
            RebelChance = new Trait(
                "conformist", "promises an afterlife of torment for those that rebel against the state",
                "rebellious", "features righteous rebels and criminals in many of its proverbs"),
            PoliticalStrength = new Trait(
                "weak", "preaches that all people are equal",
                "authoritive", "demands unquestioning obedience to authority");

        private static uint _nextSeed;

        private readonly uint _seed;

        public readonly string LowerDescription, HighDescription, LowAdjective, HighAdjective;

        public Trait(string lowerAdjective, string lowerDescription, string higherAdjective, string higherDescription)
        {
            _seed = _nextSeed++;
            LowAdjective = lowerAdjective;
            HighAdjective = higherAdjective;
            LowerDescription = lowerDescription;
            HighDescription = higherDescription;
            List.Add(this);
        }

        public string GetAdjective(double value)
        {
            return value < .5 ? LowAdjective : HighAdjective;
        }

        public double Get(Entity subject)
        {
            double result = MurmurHash.Get(_seed, subject).ToFloat();
            foreach (var influence in subject.Attributes.OfType<IAffectsTrait>().Where(a => Equals(a.DefiningTrait, this)))
            {
                result = Personality.Merge(result, influence.DefiningTraitValue);
            }
            
            var isVassal = subject.Attributes.OfType<HasBeenVassal>().SingleOrDefault(a => a.IsActive);
            if (isVassal != null)
                result = Personality.Merge(result, Get(isVassal.Target.Monarch));

            return result;
        }

        public double Get(Entity subject1, Entity subject2)
        {
            return Personality.Merge(Get(subject1), Get(subject2));
        }
    }
}
