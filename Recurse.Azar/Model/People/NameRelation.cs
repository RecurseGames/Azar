﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language;
using System.Collections.Generic;
using System.Linq;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Model.Locations;
using Recurse.Ianna.Model;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People
{
    public class NameRelation
    {
        private readonly Person _person, _observer;

        private readonly Time<Duration>? _time;

        public NameRelation(Person person, Person observer, Time<Duration>? time)
        {
            _time = time;
            _person = person;
            _observer = observer;
        }

        public string Execute()
        {
            if (Equals(_observer, _person)) return null;

            Link[] links;
            if (!GetLinks(_observer, 2, _time).TryGetValue(_person, out links))
                return null;

            if (links.SequenceEqual(new[] { Link.Parent }))
                return _person.Sexuality.ParentNoun;
            if (links.SequenceEqual(new[] { Link.Spouse }))
                return _person.Sexuality.SpouseNoun;
            if (links.SequenceEqual(new[] { Link.Child }))
                return _person.Sexuality.ChildNoun;

            if (links.SequenceEqual(new[] { Link.Parent, Link.Parent }))
                return "grand" + _person.Sexuality.ParentNoun;
            if (links.SequenceEqual(new[] { Link.Parent, Link.Child }))
                return _person.Sexuality.SiblingNoun;
            if (links.SequenceEqual(new[] { Link.Parent, Link.Spouse }))
                return "step" + _person.Sexuality.ParentNoun;
            if (links.SequenceEqual(new[] { Link.Child, Link.Child }))
                return "grand" + _person.Sexuality.ChildNoun;
            if (links.SequenceEqual(new[] { Link.Child, Link.Spouse }))
                return _person.Sexuality.ChildNoun + "-in-law";
            if (links.SequenceEqual(new[] { Link.Spouse, Link.Parent }))
                return _person.Sexuality.ParentNoun + "-in-law";
            if (links.SequenceEqual(new[] { Link.Spouse, Link.Child }))
                return "step" + _person.Sexuality.ChildNoun;

            return null;
        }
        
        private static Dictionary<Person, Link[]> GetLinks(Person observer, int maxDepth, Time<Duration>? time)
        {
            Dictionary<Person, Link[]> result = null;
            for (var depth = 1; depth <= maxDepth; depth++)
            {
                if (depth == 1)
                {
                    result = GetLinks(observer, time);
                }
                else
                {
                    foreach (var frontier in result.Where(e => e.Value.Length == depth - 1).ToList())
                    {
                        var newLinks = GetLinks(frontier.Key, maxDepth - 1, time);
                        foreach (var newLink in newLinks)
                        {
                            if (!result.ContainsKey(newLink.Key))
                                result[newLink.Key] = frontier.Value.Concat(newLink.Value).ToArray();
                        }
                    }
                }
            }
            return result ?? new Dictionary<Person, Link[]>();
        }

        private static Dictionary<Person, Link[]> GetLinks(Person observer, Time<Duration>? time)
        {
            var result = new Dictionary<Person, Link[]>();
            if (observer == null)
                return result;

            var parents = ParentageRelationship.GetParents(observer);
            foreach (var parent in parents)
                result[parent] = new[] { Link.Parent };

            var children = ParentageRelationship.GetChildren(observer);
            foreach (var child in children)
                result[child] = new[] { Link.Child };

            var spouses = observer.Attributes
                .OfType<HasRomance>()
                .Where(r => r.Romance.IsOfficial && (!time.HasValue || time.Value.CompareTo(r.Romance.Start.Time) > 0))
                .Select(r => r.Partner);
            foreach (var partner in spouses)
                result[partner] = new[] { Link.Spouse };

            return result;
        }

        private enum Link
        {
            Parent, Child, Spouse
        }
    }
}
