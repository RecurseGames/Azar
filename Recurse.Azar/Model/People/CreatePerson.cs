﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Tracking;
using System.Linq;

namespace Recurse.Azar.Model.People
{
    public static class CreatePerson
    {
        public static Person Execute(Cosmos cosmos)
        {
            var person = new Person();

            person.Sexuality = new Sexuality(cosmos.Rng);
            person.NameSyllables = NameBuilder.Syllables.CreateString(cosmos.Rng, cosmos.Rng.Next(4) + 3);

            return person;
        }
    }
}
