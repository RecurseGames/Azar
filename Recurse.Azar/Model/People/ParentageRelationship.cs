﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Records
{
    public class ParentageRelationship
    {
        public readonly Person Parent, Child;

        public readonly bool IsBiological;

        public ParentageRelationship(Person parent, Person child, bool isBiological)
        {
            IsBiological = isBiological;
            Parent = parent;
            Child = child;
        }

        public bool Contains(Person person)
        {
            return Equals(person, Parent) || Equals(person, Child);
        }

        public void Apply()
        {
            Parent.ParentageRelationships.Add(this);
            Child.ParentageRelationships.Add(this);
        }

        public static IEnumerable<Person> GetSelfAndParentsAndGrandparents(Person record)
        {
            return new[] { record }
                .Concat(GetParents(record))
                .SelectMany(p => GetParents(p).Concat(new[] { p }))
                .Distinct();
        }

        public static IEnumerable<Person> GetParents(Person record)
        {
            return record.ParentageRelationships
                .Select(r => r.Parent)
                .Where(c => !Equals(c, record));
        }

        public static IEnumerable<Person> GetChildren(Person record)
        {
            return record.ParentageRelationships
                .Select(r => r.Child)
                .Where(c => !Equals(c, record));
        }
    }
}
