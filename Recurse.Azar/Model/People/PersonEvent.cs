﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People
{
    public abstract class PersonEvent : MutableEvent<Time<Duration>?>
    {
        protected readonly Person Person;

        protected readonly Cosmos Cosmos;

        public PersonEvent(Cosmos cosmos, Person person)
        {
            Cosmos = cosmos;
            Person = person;
        }
    }
}
