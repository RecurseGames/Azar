﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People.Records
{
    public class LocationRecord
    {
        private readonly Dictionary<Time<Duration>, Location> _locations = 
            new Dictionary<Time<Duration>, Location>();

        private readonly Entity _owner;

        public LocationRecord(Entity owner)
        {
            _owner = owner;
        }

        public Location Get()
        {
            return _locations.OrderBy(e => e.Key).LastOrDefault().Value;
        }

        public Location Get(Time<Duration> time)
        {
            var result = _locations
                .Where(e => e.Key.CompareTo(time) < 0)
                .OrderBy(e => time.GetDifference(e.Key))
                .FirstOrDefault();
            return result.Value;
        }

        public void Set(Time<Duration> time, Location location)
        {
            _locations[time] = location;
            if (location != null)
                location.PastContents.Add(_owner);
        }
    }
}
