﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Hashing;
using Recurse.Azar.Model.Hatred;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using System;

namespace Recurse.Azar.Model.People
{
    public static class Personality
    {
        public static double Merge(double a, double b)
        {
            // Squaring two random numbers and taking the highest has a very similar
            // distribution to simply generating a random number (I found this empirically).
            // This is useful to generating random relationship behaviour while respecting
            // the tendencies of those involved.
            return Math.Max(Math.Pow(a, 2), Math.Pow(b, 2));
        }

        public static bool TestJealousy(Cosmos cosmos, Person person, Person target, ActionRecord cause)
        {
            var chance = Trait.Jealousy.Get(person);
            chance = chance < .9 ? 0 : Math.Pow((chance - .9) * 10, 2);
            if (cosmos.Rng.NextDouble() < chance)
            {
                var feud = new Feud(person);
                Feud.List.TryAdd(cosmos.Tracker, feud);

                person.Events.TryAdd(new LifecycleEvent
                {
                    Stage = new StartEmnity(
                       cosmos,
                       new Emnity(person, target),
                       feud,
                       cause)
                });
                return true;
            }
            return false;
        }
    }
}
