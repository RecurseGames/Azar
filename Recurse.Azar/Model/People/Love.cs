﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Sex.Romances;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.People
{
    public static class Love
    {
        public static Dictionary<Person, double> GetTowards(Person person)
        {
            var result = new Dictionary<Person, double>();
            foreach (var romance in person.Attributes.OfType<HasRomance>().Select(r => r.Romance))
            {
                var partner = romance.GetPartnerOrNull(person);
                result[partner] = Trait.ClosenessToUnofficialPartners.Get(partner);
            }
            foreach (var child in ParentageRelationship.GetChildren(person).Where(c => !c.IsDead))
                result[child] = .8;
            foreach (var parent in ParentageRelationship.GetParents(person).Where(p => !p.IsDead))
                result[parent] = 1;
            return result.Where(e => !e.Key.IsDead).ToDictionary(e => e.Key, e => e.Value);
        }
    }
}
