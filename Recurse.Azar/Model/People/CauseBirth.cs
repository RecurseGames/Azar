﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Residency;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Tracking;
using System.Linq;

namespace Recurse.Azar.Model.Sex.Reproduction
{
    public static class CauseBirth
    {
        public static readonly TrackedList<Person> All = new TrackedList<Person>("People");

        public static Birth Execute(
            Cosmos cosmos,
            Person child,
            Person mother,
            Person father = null)
        {
            return Execute(cosmos, child, mother.Location.Get(), mother, father);
        }

        public static Birth Execute(
            Cosmos cosmos,
            Person child,
            Location location)
        {
            return Execute(cosmos, child, location, null, null);
        }

        private static Birth Execute(
            Cosmos cosmos,
            Person child,
            Location location,
            Person mother,
            Person father)
        {
            // TODO: Need aliases, e.g., "X's son"

            // Brian's father was Carl, a peach farmer from Fangorn, who was in Eoghad at the time.
            // Brian's parents were Anna, a peach farmer; and Carl, a peach farmer, who was by now quite old.
            // Brian's biological parents were Gloria, a peach farmer; and Carl, a peach farmer;

            var isBorn = new IsBorn(cosmos.Time, location);
            child.Attributes.Add(isBorn);

            var instant = cosmos.CreateInstant();
            child.Location.Set(instant.Time, location);

            var record = new BirthRecord(
                instant.Time,
                location,
                child,
                mother,
                father);
            child.Records.Add(record);
            if (mother != null)
            {
                mother.Records.Add(record);
            }
            if (father != null)
            {
                father.Records.Add(record);
            }
            
            var birth = new Birth();
            birth.Start = record;

            child.Records.Add(new ResidenceRecord(
                instant,
                location));

            if (mother != null)
                new ParentageRelationship(mother, child, true).Apply();
            if (father != null)
                new ParentageRelationship(father, child, true).Apply();

            All.TryAdd(cosmos.Tracker, child);

            return birth;
        }
    }
}
