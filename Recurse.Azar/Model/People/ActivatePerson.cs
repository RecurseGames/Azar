﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Occupations;
using Recurse.Azar.Model.People.Events;

namespace Recurse.Azar.Model.People
{
    public static class ActivatePerson
    {
        public static void Execute(Cosmos cosmos, Person person)
        {
            person.HealthLifecycle.Stage = new Health(cosmos, person);
            person.Events.TryAdd(new RomancesUpdateEvent(cosmos, person));
            person.Events.TryAdd(new ConsiderEmigration(cosmos, person));
            person.Events.TryAdd(new AdoptOccupation(cosmos, person));
            person.Events.TryAdd(new FoundPhilosophyEvent(cosmos, person));
            person.Events.TryAdd(new AssumeLeadershipEvent(cosmos, person));
            person.Events.TryAdd(new RecruitEvent(cosmos, person));

            cosmos.Events.TryAdd(person.Events);
        }
    }
}
