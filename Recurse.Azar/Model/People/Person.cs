﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Sex;
using System.Linq;
using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using System.Collections.Generic;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Occupations;
using Recurse.Azar.Model.Hatred;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Model.Politics;
using Recurse.Azar.Model.Locations;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People
{
    public class Person : Entity
    {
        public Sexuality Sexuality { get; set; }

        public Bloodline Bloodline { get; set; }

        public Syllable[] NameSyllables
        {
            get; set;
        }

        public readonly List<ParentageRelationship> ParentageRelationships = new List<ParentageRelationship>();

        public readonly LifecycleEvent HealthLifecycle = new LifecycleEvent();
        
        public string FirstName
        {
            get
            {
                return NameSyllables != null && NameSyllables.Any() ?
                    StringUtils.TitleCase(
                        string.Join("", NameSyllables.Select(s => s.Text).ToArray())) :
                        null;
            }
        }

        public string Name
        {
            get
            {
                var firstName = FirstName;
                if (firstName == null)
                {
                    var result = "an unnamed person";
                    if (Bloodline != null)
                        result += " of the " + Bloodline.Surname + " family";
                    return result;
                }
                if (Bloodline != null)
                    return firstName + " " + Bloodline.Surname;
                return firstName;
            }
        }

        public Person() : base(true)
        {
            Events.TryAdd(HealthLifecycle);
            Sexuality = Sexuality.Default;
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            if (ParentageRelationships.Any(p => p.Contains(relative as Person)))
            {
                yield return RecordType.Marriage;
                yield return RecordType.Conversion;
                yield return RecordType.Political;
                yield return RecordType.Birth;
                yield return RecordType.Death;
            }
            if (Attributes.OfType<HasRomance>()
                .Any(a =>
                Equals(a.Partner, relative) &&
                a.Romance.End.HasValue &&
                a.Romance.Start.Time.CompareTo(time) <= 0 &&
                a.Romance.End.Value.CompareTo(time) >= 0))
            {
                yield return RecordType.Political;
                yield return RecordType.Birth;
                yield return RecordType.Death;
            }
            yield break;
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            foreach (var child in ParentageRelationship.GetChildren(this))
                yield return child;
            foreach (var parent in ParentageRelationship.GetParents(this))
                yield return parent;
            foreach (var spouse in Attributes.OfType<HasRomance>().Select(a => a.Partner))
                yield return spouse;
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            return GetIntroduction(context);
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            var philosophy = HasPhilosophy.Check(this);
            var adjective = philosophy != null ?
                philosophy.Adjective :
                null;

            var identities = new List<Hyperstring>();
            identities.AddRange(Attributes
                .OfType<HasHeldOffice>()
                .OrderByDescending(a => a.Importance)
                .Select(a => a.Title.GetReference(context))
                .Select(r => r.GetNoun(PronounType.They).Hyperstring));
            var hasOccupation = HasOccupation.Get(this);
            if (hasOccupation != null)
                identities.Add(hasOccupation.Occupation.Name);
            if (Attributes.OfType<IsMurderer>().Any())
                identities.Add("murderer");
            if (Attributes.Contains(Attribute.IsRebel))
                identities.Add("rebel");

            if (identities.Any() || adjective != null)
            {
                yield return new CategoryRecord(
                    this,
                    adjective,
                    identities.ToArray());
            }
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(aliases, FirstName, Sexuality.Pronouns, this);
        }

        protected override IGrammarObject CreateReference(TextContext context)
        {
            if (context.Referenced.Add(this))
            {
                var observer = context.Focus as Person;
                var nameRelation = new NameRelation(this, observer, context.Time);
                var relation = nameRelation.Execute();
                if (relation != null)
                {
                    var relationAlias = new OwnedGrammarObject(
                        observer.GetSimpleReference(context.Aliases),
                        relation);
                    context.Aliases.Add(relationAlias, this);

                    return new TitledGrammarObject(
                        relationAlias,
                        new NamedGrammarObject(context.Aliases, Name, Sexuality.Pronouns, this));
                }
                else if (context.Time.HasValue && Name != null)
                {
                    var age = Age.TryGet(this, context.Time.Value);
                    if (age.HasValue)
                    {
                        var noun = (age.Value / Durations.Year) >= 18 ? Sexuality.AdultNoun : Sexuality.JuvenileNoun;
                        return new NamedGrammarObject(context.Aliases, Name, Sexuality.Pronouns, this)
                        {
                            Adjective = Hyperstring.Empty + noun + "named"
                        };
                    }
                }
                return new NamedGrammarObject(context.Aliases, Name, Sexuality.Pronouns, this);
            }
            return base.CreateReference(context);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
