﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Lifecycles
{
    public abstract class Stage
    {
        private Action<Stage> _transition;

        public abstract IEnumerable<Occurence> GetOccurences();

        public void Start(Action<Stage> transition)
        {
            if (_transition != null)
                throw new InvalidOperationException("Stage already started.");
            _transition = transition;
            OnStart();
        }

        protected abstract void OnStart();

        protected void Transition(Stage newStage)
        {
            if (_transition == null)
                throw new InvalidOperationException("Stage hasn't started yet.");
            _transition(newStage);
        }

        protected Occurence CreateTransition(Stage newStage, Time<Duration> time)
        {
            return new Occurence(
                () => _transition(newStage),
                time);
        }
    }
}
