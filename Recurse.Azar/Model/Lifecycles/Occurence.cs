﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Lifecycles
{
    public struct Occurence
    {
        private readonly Action _action;

        private readonly Time<Duration> _time;

        public Action Action
        {
            get { return _action; }
        }

        public Time<Duration> Time
        {
            get { return _time; }
        }

        public Occurence(Action next, Time<Duration> time)
        {
            _action = next;
            _time = time;
        }

        public static Occurence Create<T>(Action<T> setItem, T item, Time<Duration> time)
        {
            return new Occurence(() => setItem(item), time);
        }
    }
}
