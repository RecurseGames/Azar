﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using System;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Lifecycles
{
    public class LifecycleEvent : MutableEvent<Time<Duration>?>
    {
        private Stage _stage;

        private Action _nextAction;

        public Stage Stage
        {
            get { return _stage; }
            set
            {
                if (Equals(Stage, value)) return;
                _stage = value;

                if (Stage != null)
                    Stage.Start(SetStage);

                Reschedule();
            }
        }

        public override void Occur()
        {
            if (_nextAction != null)
                _nextAction();
            Reschedule();
        }

        private void SetStage(Stage stage)
        {
            Stage = stage;
        }

        private void Reschedule()
        {
            if (Stage == null)
            {
                Time = null;
                IsFixed = true;
                return;
            }

            var occurence = Stage
                .GetOccurences()
                .OrderBy(o => o.Time)
                .FirstOrDefault();
            _nextAction = occurence.Action;
            if (_nextAction != null)
            {
                if (double.IsNaN(occurence.Time.OriginDistance / Durations.Year))
                    throw new InvalidOperationException();

                Time = occurence.Time;
            }
            else
            {
                Time = null;
            }
        }
    }
}
