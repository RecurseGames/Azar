﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using System.Collections.Generic;
using System.Linq;
using Recurse.Ianna.Model;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Records
{
    public class ActionRecord : IRecord
    {
        #region Backing Fields

        private readonly ICollection<CausalRelationship> _causalRelationships =
            new HashSet<CausalRelationship>();

        private readonly List<IRecord> _errata = new List<IRecord>();

        private readonly List<Entity>
            _subjects,
            _objects;

        private readonly GrammarAction _action;

        private readonly Time<Duration> _time;

        #endregion

        public RecordType Type { get; set; }

        public List<Entity> Subjects
        {
            get { return _subjects; }
        }
        
        public List<Entity> Objects
        {
            get { return _objects; }
        }

        public Time<Duration> Time
        {
            get { return _time; }
        }

        Time<Duration>? IRecord.Time
        {
            get { return Time; }
        }

        public Location Location
        {
            get
            {
                if (Subjects == null)
                    return Objects != null ? Entity.GetLocation(Objects) : null;
                if (Objects == null)
                    return Subjects != null ? Entity.GetLocation(Subjects) : null;
                return Entity.GetLocation(Subjects.Concat(Objects));
            }
        }

        public GrammarAction Action
        {
            get { return _action; }
        }

        IEnumerable<IRecord> IRecord.Errata
        {
            get { return Errata; }
        }

        public List<IRecord> Errata
        {
            get { return _errata; }
        }

        public ICollection<CausalRelationship> CausalRelationships
        {
            get { return _causalRelationships; }
        }

        public IEnumerable<Entity> Mentions
        {
            get { return (Subjects ?? Enumerable.Empty<Entity>()).Concat(Objects ?? Enumerable.Empty<Entity>()); }
        }

        public ActionRecord(Instant instant, GrammarAction action) : this(instant.Time, action)
        {
        }

        public ActionRecord(Time<Duration> time, GrammarAction action)
        {
            _time = time;
            _action = action;
            _subjects = action != null ? new List<Entity>() : null;
            _objects = action != null && action.RequiredTransitivity != false ? new List<Entity>() : null;
        }

        public Hyperstring GetPrimaryClause(
            TextContext context,
            GrammarCategory category)
        {
            if (Action == null)
                return null;

            if (Objects != null && 
                Objects.Contains(context.Focus) &&
                Action.Inverse != null)
            {
                return Action.Inverse.ToSequence(
                    Entity.GetReference(Objects, context),
                    Entity.GetReference(Subjects, context),
                    category);
            }

            IEnumerable<Entity> subjects = Subjects;
            List<Entity> with = null;
            if (subjects.Contains(context.Focus))
            {
                with = subjects.Where(s => !Equals(s, context.Focus)).ToList();
                subjects = new[] { context.Focus };
            }

            Hyperstring result = Action.ToSequence(
                Entity.GetReference(subjects, context),
                Entity.GetReference(Objects, context),
                category);
            if (with != null && with.Any())
            {
                result += "with" + Entity.GetReference(with, context)
                    .GetNoun(PronounType.Them).Hyperstring;

                if (Objects == null || Objects.Count() <= 1)
                {
                    context.Aliases.Add(Pronoun.Neutral, Entity.GetReference(Subjects, context));
                }
            }
            return result;
        }
    }
}
