﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Records
{
    public struct CausalRelationship
    {
        public readonly IRecord Cause, Effect;

        public CausalRelationship(IRecord cause, IRecord effect)
        {
            Cause = cause;
            Effect = effect;
        }

        public void Apply()
        {
            Cause.CausalRelationships.Add(this);
            Effect.CausalRelationships.Add(this);
        }

        public static IEnumerable<IRecord> GetCauses(IRecord record)
        {
            return record.CausalRelationships
                .Select(r => r.Cause)
                .Where(c => !Equals(c, record));
        }

        public static IEnumerable<IRecord> GetEffects(IRecord record)
        {
            return record.CausalRelationships
                .Select(r => r.Effect)
                .Where(c => !Equals(c, record));
        }
    }
}
