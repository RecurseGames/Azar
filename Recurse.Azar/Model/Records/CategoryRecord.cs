﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Verbs;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Records
{
    public class CategoryRecord : IRecord
    {
        private readonly List<CausalRelationship> _causalRelationships = new List<CausalRelationship>();

        private readonly Entity _subject;

        private readonly Hyperstring _adjective;

        private readonly Hyperstring _categories;

        public IEnumerable<Entity> Mentions
        {
            get { yield return _subject; }
        }

        public RecordType Type { get; set; }

        public Location Location
        {
            get { return null; }
        }

        public IEnumerable<IRecord> Errata
        {
            get { yield break; }
        }

        public Time<Duration>? Time
        {
            get { return null; }
        }

        public Measurement<Duration>? Duration
        {
            get; set;
        }

        public ICollection<CausalRelationship> CausalRelationships
        {
            get { return _causalRelationships; }
        }

        public CategoryRecord(Entity subject, Hyperstring adjective, params Hyperstring[] categories)
        {
            var delimitedCategories = new DelimitedString
            {
                Delimiter = RenderSymbol.Comma,
                FinalDelimiter = "and"
            };
            delimitedCategories.Symbols.AddRange(categories);

            _subject = subject;
            _adjective = adjective;
            _categories = new Hyperstring.Simple(delimitedCategories);
        }

        public Hyperstring GetPrimaryClause(TextContext context, GrammarCategory category)
        {
            var noun = _subject.GetReference(context).GetNoun(PronounType.They);
            var verb = noun.IsPlural ?
                (_subject.IsDead ? "were a" : "are a") :
                (_subject.IsDead ? "was a" : "is a");
            var result =
                Hyperstring.Empty +
                noun.Hyperstring +
                verb +
                (_adjective ?? Hyperstring.Empty) +
                _categories;
            if (Duration.HasValue)
            {
                result += Hyperstring.Empty + "for" + DurationText.AsDuration(Duration.Value, true);
            }
            return result;
        }
    }
}
