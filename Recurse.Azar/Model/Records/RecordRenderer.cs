﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Text;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Records
{
    public class RecordRenderer
    {
        private HashSet<IRecord>
            _lastOutput = new HashSet<IRecord>(),
            _secondLastOutput = new HashSet<IRecord>();

        public readonly TextContext Context = new TextContext();

        public bool OutputAsides { get; set; }

        public IEnumerable<IRecord> RecentOutput
        {
            get { return _lastOutput.Concat(_secondLastOutput); }
        }

        public Hyperstring TryRender(IRecord record)
        {
            if (RecentOutput.Contains(record))
                return null;
            Context.Aliases.ClearAmbiguities();

            var result = TryGetSentence(record, GrammarCategory.SimplePast, false, true);
            if (result != null)
                result += GetAside(record);

            _secondLastOutput = _lastOutput;
            _lastOutput = new HashSet<IRecord>();

            return result;
        }
        
        private Hyperstring TryGetSentence(
            IRecord record, 
            GrammarCategory category, 
            bool sceneIsRelative,
            bool includeCause,
            bool includePeriod = true)
        {
            if (!_lastOutput.Add(record)) return null;

            bool newlineNeeded;
            var originalLocation = Context.Location;
            var originalTime = Context.Time;
            var sceneClause = Context.TryGetSceneClause(
                record.Location,
                record.Time,
                sceneIsRelative,
                out newlineNeeded);

            if (newlineNeeded)
            {
                Context.Aliases.Remove(Pronoun.List.Cast<IGrammarAlias>());
            }

            var result = record.GetPrimaryClause(Context, category);
            if (result == null)
            {
                Context.Location = originalLocation;
                Context.Time = originalTime;
                return null;
            }

            if (sceneClause != null)
            {
                result = Context.PrependScene ?
                    (sceneClause + RenderSymbol.Comma + result) : 
                    (result + sceneClause);
                Context.PrependScene = !Context.PrependScene;
            }
            else if (!sceneIsRelative && record.Time.HasValue)
            {
                result = Hyperstring.Empty + "Then" + RenderSymbol.Comma + result;
            }

            var cause = includeCause ? CausalRelationship.GetCauses(record).SingleOrDefault() : null;
            if (cause != null)
            {
                var causeClause = cause.GetPrimaryClause(Context, GrammarCategory.SimplePast);
                if (causeClause != null)
                {
                    if (Equals(cause.Time, record.Time))
                    {
                        if (sceneClause != null)
                            result += RenderSymbol.Comma;
                        result += "when" + causeClause;
                    }
                    else if (!sceneIsRelative)
                    {
                        if (sceneClause != null)
                            result += RenderSymbol.Comma;
                        result += "because" + causeClause;

                        var causeSceneClause = Context.TryGetRelativeSceneClause(
                            cause.Location,
                            cause.Time);
                        if (causeSceneClause != null)
                            result += causeSceneClause;
                    }
                }
            }

            if (includePeriod)
                result += RenderSymbol.Period;

            if (!OutputAsides)
                newlineNeeded = false;
            if (newlineNeeded)
                result = Hyperstring.Empty + RenderSymbol.NewLine + result;

            return result;
        }

        private Hyperstring GetAside(
            IRecord record)
        {
            Hyperstring result = Hyperstring.Empty;
            
            foreach (var mention in record.Mentions)
            {
                if (Equals(mention, Context.Focus)) continue;
                if (Context.Introductions.Add(mention))
                {
                    foreach (var introRecord in mention.TryGetShortIntroduction(Context))
                    {
                        var sentence = TryGetSentence(introRecord, GrammarCategory.SimplePast, true, true);
                        if (sentence != null)
                        {
                            OutputAsides = true;
                            result += sentence;
                        }
                    }
                }
            }
            
            var effects = new List<Hyperstring>();
            foreach (var effect in CausalRelationship.GetEffects(record))
            {
                var sentence = TryGetSentence(
                    effect,
                    Equals(effect.Time, record.Time) ?
                        GrammarCategory.SimplePast :
                        GrammarCategory.SimplePastFuture,
                    true,
                    false,
                    false);
                if (sentence != null)
                {
                    effects.Add(sentence);
                }
            }
            if (effects.Any())
            {
                OutputAsides = true;

                result += "as a result";
                result += RenderSymbol.Comma;

                var resultsContainComma = effects.Any(r => r.GetFlattened().Contains(RenderSymbol.Comma));
                var delimiter = resultsContainComma ? RenderSymbol.Semicolon : RenderSymbol.Comma;
                var delimitedString = new DelimitedString
                {
                    Delimiter = delimiter,
                    FinalDelimiter = new Hyperstring.Simple(delimiter, "and")
                };
                delimitedString.Symbols.AddRange(effects.Cast<Hypersymbol>());

                if (new Hyperstring.Simple(delimitedString).Count() == 0)
                    throw new System.Exception();

                result += new Hyperstring.Simple(delimitedString) + RenderSymbol.Period;
            }

            foreach (var errata in record.Errata)
            {
                var sentence = TryGetSentence(errata, GrammarCategory.SimplePast, true, true);
                if (sentence != null)
                {
                    OutputAsides = true;
                    result += sentence;
                }
            }

            return result;
        }
    }
}
