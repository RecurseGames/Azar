﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Recurse.Common.Text;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Model.Records
{
    public class GenericNameWithDuration : IGrammarObject
    {
        private readonly Entity _entity;

        private readonly Cosmos _cosmos;

        private readonly string _genericName;

        public GenericNameWithDuration(Cosmos cosmos, Entity entity, string genericName)
        {
            _cosmos = cosmos;
            _entity = entity;
            _genericName = genericName;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            Hyperstring result = "a";

            var age = Age.TryGet(_entity, _cosmos.Time);
            if (age.HasValue)
            {
                result += DurationText.AsDuration(age.Value, false);
            }
            result += _genericName;
            return result;
        }
    }
}
