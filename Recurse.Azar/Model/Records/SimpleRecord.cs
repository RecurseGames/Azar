﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Records
{
    public class SimpleRecord : IRecord
    {
        private readonly List<CausalRelationship> _causalRelationships = new List<CausalRelationship>();

        private readonly Hyperstring _clause;

        public IEnumerable<Entity> Mentions
        {
            get { yield break; }
        }

        public Location Location
        {
            get { return null; }
        }

        public IEnumerable<IRecord> Errata
        {
            get { yield break; }
        }

        public Time<Duration>? Time
        {
            get { return null; }
        }

        public RecordType Type { get; set; }

        public Measurement<Duration>? Duration
        {
            get; set;
        }

        public ICollection<CausalRelationship> CausalRelationships
        {
            get { return _causalRelationships; }
        }

        public SimpleRecord(Hyperstring clause)
        {
            _clause = clause;
        }

        public Hyperstring GetPrimaryClause(TextContext context, GrammarCategory category)
        {
            return _clause;
        }
    }
}
