﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Collections;
using Recurse.Common.Emitters;
using Recurse.Azar.Model.Records;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model
{
    public class RecordSet
    {
        private readonly HashSet<IRecord> _records = new HashSet<IRecord>();

        public readonly List<Action<IRecord>> Handlers = new List<Action<IRecord>>();

        public IEnumerable<T> OfType<T>() where T : IRecord
        {
            return _records.OfType<T>().OrderBy(r => r.Time);
        }

        public T LastOrDefault<T>(Func<T, bool> predicate = null) where T : IRecord
        {
            IEnumerable<T> result = OfType<T>();
            if (predicate != null) result = result.Where(predicate);
            return result.LastOrDefault();
        }

        public void Add(IRecord item)
        {
            _records.Add(item);
            foreach (var handler in Handlers)
            {
                try
                {
                    handler(item);
                }
                catch (Exception e)
                {
                    throw new ObserverException(e);
                }
            }
        }
    }
}
