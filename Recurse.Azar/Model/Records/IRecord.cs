﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Records
{
    public interface IRecord
    {
        IEnumerable<IRecord> Errata { get; }

        IEnumerable<Entity> Mentions { get; }

        Location Location { get; }

        Time<Duration>? Time { get; }

        RecordType Type { get; }

        ICollection<CausalRelationship> CausalRelationships { get; }

        Hyperstring GetPrimaryClause(
            TextContext context,
            GrammarCategory category);
    }
}
