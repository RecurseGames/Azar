﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Model;
using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Language
{
    public class TempEntity : Entity, IGrammarObject
    {
        private readonly Hyperstring _text;

        public TempEntity(Hyperstring text)
        {
            _text = text;
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield break;
        }

        public GrammarNoun GetNoun(PronounType type)
        {
            return _text;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return this;
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            throw new InvalidOperationException();
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            yield break;
        }
    }
}
