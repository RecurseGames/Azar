﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;

namespace Recurse.Azar.Model
{
    public class RenderSymbol : Hypersymbol
    {
        public static Hypersymbol
            Comma = new RenderSymbol
            {
                Text = ",",
                NewSentenceAfter = false
            },
            Semicolon = new RenderSymbol
            {
                Text = ";",
                NewSentenceAfter = false
            },
            Period = new RenderSymbol
            {
                Text = ".",
                NewSentenceAfter = true
            },
            PossessiveSuffix = new RenderSymbol
            {
                Text = "'s",
                NewSentenceAfter = false
            },
            NewLine = new RenderSymbol
            {
                Text = "\n\n",
                NewSentenceAfter = true
            },
            Null = new RenderSymbol
            {
                Text = "NULL"
            };
        
        public string Text { get; set; }

        public bool? NewSentenceAfter { get; set; }

        public bool FollowsSpace { get; set; }
    }
}
