﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Text
{
    public class TextContext
    {
        public Time<Duration>? Time { get; set; }

        public Location Location { get; set; }

        public Entity Focus { get; set; }

        public readonly Dictionary<Entity, int> NotedAges = new Dictionary<Entity, int>();

        public bool PrependScene { get; set; }

        public readonly AliasMapping Aliases = new AliasMapping();

        public readonly HashSet<Entity> Introductions = new HashSet<Entity>();

        public readonly HashSet<Entity> Referenced = new HashSet<Entity>();

        public Hyperstring TryGetRelativeSceneClause(
            Location location,
            Time<Duration>? time)
        {
            bool newlineNeeded;
            var result = TryGetSceneClause(location, time, true, out newlineNeeded);
            if (newlineNeeded)
                throw new InvalidOperationException();
            return result;
        }

        public Hyperstring TryGetSceneClause(
            Location location,
            Time<Duration>? time,
            bool isRelative,
            out bool newlineNeeded)
        {
            bool prependTime = false;
            var timeString = TryGetTimeString(time, isRelative, out newlineNeeded, ref prependTime);
            if (location != null && !Equals(location, Location))
            {
                var result = "in" + location.GetReference(this).GetNoun(PronounType.Them).Hyperstring;
                if (timeString != null)
                {
                    if (prependTime)
                        result = timeString + RenderSymbol.Comma + result;
                    else
                        result += timeString;
                }
                Location = location ?? Location;
                return result;
            }
            return timeString;
        }

        private Hyperstring TryGetTimeString(
            Time<Duration>? time,
            bool isRelative,
            out bool newlineNeeded,
            ref bool prependTime)
        {
            if (!time.HasValue)
            {
                newlineNeeded = false;
                return null;
            }
            if (isRelative)
            {
                newlineNeeded = false;
                return TryGetRelativeTimeString(time.Value);
            }
            else
            {
                return TryGetNewTimeString(time.Value, out newlineNeeded, ref prependTime);
            }
        }

        private Hyperstring TryGetNewTimeString(
            Time<Duration> time,
            out bool newParagraph,
            ref bool prependTime)
        {
            Hyperstring result;
            if (Time.HasValue)
            {
                var seasonDiff = Season.GetNumber(time) - Season.GetNumber(Time.Value);
                if (seasonDiff == 4)
                {
                    newParagraph = false;
                    result = "next " + Season.Get(time).Name;
                }
                else if (seasonDiff == 0)
                {
                    newParagraph = false;
                    prependTime = true;
                    PrependScene = true;
                    result =  "then";
                }
                else if (seasonDiff > 0 && seasonDiff < (Season.List.Count - 1 - Season.GetNumber(time) % Season.List.Count))
                {
                    newParagraph = false;
                    result = "that " + Season.Get(time).Name;
                }
                else
                {
                    newParagraph = true;
                    result = GetTimeString(time);
                }
            }
            else
            {
                newParagraph = false;
                result = GetTimeString(time);
            }
            Time = time;
            return result;
        }

        private Hyperstring TryGetRelativeTimeString(
            Time<Duration> time)
        {
            if (!Time.HasValue)
                return null;

            if (Time.Value.CompareTo(time) < 0)
            {
                return
                    Hyperstring.Empty +
                    DurationText.AsDuration(time.GetDifference(Time.Value), true) +
                    " later";
            }
            if (Time.Value.CompareTo(time) > 0)
            {
                return
                    Hyperstring.Empty +
                    DurationText.AsDuration(Time.Value.GetDifference(time), true) +
                    " earlier";
            }
            return null;
        }

        private static Hyperstring GetTimeString(
            Time<Duration> time)
        {
            return
                Hyperstring.Empty +
                "during the" +
                Season.Get(time).Name +
                "of " +
                Durations.GetYear(time).ToString();
        }
    }
}
