﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Text
{
    public static class DurationText
    {
        public static string AsRepresentativePopulation(int simulatedPeople)
        {
            if (simulatedPeople < 0)
                throw new ArgumentOutOfRangeException("simulatedPeople", "Count must be 0 or greater");

            simulatedPeople *= 10;

            var result = simulatedPeople.ToString();
            result = result.Substring(0, 1).PadRight(result.Length, '0');
            for (var i = result.Length - 3; i > 0; i -= 3)
                result = result.Insert(i, ",");

            return "over " + result;
        }

        public static string AsDuration(Measurement<Duration> duration, bool usePlurals)
        {
            var years = duration / Durations.Year;
            if (years >= 1)
            {
                if (years < 3)
                    years = Math.Round(years * 2) / 2;
                return AsDuration(years, "year", usePlurals);
            }
            var months = duration / Durations.Month;
            if (months >= 1)
            {
                return AsDuration(months, "month", usePlurals);
            }
            var weeks = duration / Durations.Week;
            if (weeks >= 1)
            {
                return AsDuration(weeks, "week", usePlurals);
            }
            return AsDuration(Math.Max(1, Math.Round(duration / Durations.Day)), "day", usePlurals);
        }

        private static string AsDuration(double duration, string unit, bool usePlurals)
        {
            var roundedDuration = Math.Round(duration);
            if (roundedDuration == 1) usePlurals = false;
            return roundedDuration + " " + (usePlurals ? unit + "s" : unit);
        }
    }
}
