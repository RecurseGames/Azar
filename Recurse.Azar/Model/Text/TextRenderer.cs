﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Common.Utils;
using System;
using System.Linq;

namespace Recurse.Azar.Model.Text
{
    public class TextRenderer
    {
        public virtual string GetString(Hyperstring s)
        {
            s = Render(s);

            var result = string.Empty;

            var newSentence = true;
            foreach (var symbol in s.GetFlattened())
            {
                var renderSymbol = symbol as RenderSymbol;
                if (renderSymbol != null)
                {
                    if (result.Any() && !char.IsWhiteSpace(result.Last()) && renderSymbol.FollowsSpace)
                        result += " ";
                    result += newSentence ? StringUtils.Capitalise(renderSymbol.Text) : renderSymbol.Text;
                    if (renderSymbol.NewSentenceAfter.HasValue)
                        newSentence = renderSymbol.NewSentenceAfter.Value;
                }
                else
                {
                    throw new ArgumentException("Hyperstring must contain only RenderSymbols");
                }
            }

            return result.Replace("s's", "s'");
        }

        protected virtual Hypersymbol Render(Hypersymbol symbol)
        {
            if (symbol == null)
                return RenderSymbol.Null;

            var renderSymbol = symbol as RenderSymbol;
            if (renderSymbol != null)
                return renderSymbol;

            var subsequence = symbol as Hyperstring;
            if (subsequence != null)
                return Render(subsequence);

            var fromString = symbol as Hypersymbol.FromString;
            if (fromString != null)
                return new RenderSymbol
                {
                    Text = fromString.String,
                    NewSentenceAfter = false,
                    FollowsSpace = true
                };

            return symbol;
        }

        private Hyperstring Render(Hyperstring s)
        {
            return new Hyperstring.Simple(s.Select(Render));
        }
    }
}
