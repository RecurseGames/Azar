﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Common.Text;

namespace Recurse.Azar.Model
{
    public class ReferenceSymbol : Hyperstring.Simple
    {
        public readonly object Target;

        public ReferenceSymbol(object target, Hyperstring wrapped) :
            base((IEnumerable<Hypersymbol>)wrapped)
        {
            Target = target;
        }
    }
}
