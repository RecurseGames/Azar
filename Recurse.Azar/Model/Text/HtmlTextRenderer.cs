﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using System.Collections.Generic;

namespace Recurse.Azar.Model.Text
{
    public class HtmlTextRenderer : TextRenderer
    {
        private readonly Dictionary<object, int> _ids = new Dictionary<object, int>();

        private int _nextId;

        public string GetId(object o)
        {
            if (!_ids.ContainsKey(o))
                _ids[o] = _nextId++;
            return "e" + _ids[o];
        }

        public override string GetString(Hyperstring s)
        {
            return base.GetString(s).Replace("\n", "<br/>");
        }

        protected override Hypersymbol Render(Hypersymbol symbol)
        {
            var reference = symbol as ReferenceSymbol;
            if (reference == null)
                return base.Render(symbol);

            return
                Hyperstring.Empty +
                new RenderSymbol { Text = " <a href='" + GetId(reference.Target) + ".html'>" } +
                base.Render(symbol) +
                new RenderSymbol { Text = "</a>" };
        }
    }
}
