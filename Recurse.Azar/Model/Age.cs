﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Model.Text;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model
{
    public static class Age
    {
        public static Measurement<Duration>? TryGet(Entity entity, Time<Duration> time)
        {
            var isBorn = entity.Attributes.OfType<IsBorn>().SingleOrDefault();
            if (isBorn == null) return null;
            return time.GetDifference(isBorn.TimeOfBirth);
        }

        public static Measurement<Duration> Get(Entity entity, Time<Duration> time)
        {
            return TryGet(entity, time).Value;
        }

        public static double GetYears(Entity entity, Time<Duration> time)
        {
            return Get(entity, time) / Durations.Year;
        }

        public static ActionRecord Describe(Person entity, Time<Duration> time)
        {
            var age = Get(entity, time);
            return new ActionRecord(time, new IsAdjective(DurationText.AsDuration(age, true) + " old"))
            {
                Subjects = { entity }
            };
        }
    }
}
