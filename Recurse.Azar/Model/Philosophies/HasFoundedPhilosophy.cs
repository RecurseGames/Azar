﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Politics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recurse.Azar.Model.Philosophies
{
    public class HasFoundedPhilosophy : IDenotesTitle
    {
        private readonly Philosophy _philosophy;

        public double Importance
        {
            get { return 2.5; }
        }

        public NobleTitle Title
        {
            get { return new NobleTitle(_philosophy) { Rank = "Founder" }; }
        }

        public bool IsActive
        {
            get { return true; }
        }

        public HasFoundedPhilosophy(Philosophy philosophy)
        {
            _philosophy = philosophy;
        }
    }
}
