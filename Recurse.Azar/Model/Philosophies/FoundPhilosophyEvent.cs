﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Philosophies;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Occupations
{
    /// <summary>
    /// This event is for people who are destined to found a philosophical movement
    /// </summary>
    public class FoundPhilosophyEvent : MutableEvent<Time<Duration>?>
    {
        private readonly Person _person;

        private readonly Cosmos _cosmos;

        public FoundPhilosophyEvent(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;
            Time = cosmos.Time + GeometricDistribution.GetNext(cosmos.Rng, .5) * 30 * Durations.Year;
        }

        public override void Occur()
        {
            var hasOccupation = HasOccupation.Get(_person);
            var occupation = hasOccupation != null ? hasOccupation.Occupation : null;
            if (occupation != null &&
                _cosmos.Rng.NextDouble() < occupation.RelativeStartPhilosophyChance)
            {
                var newPhilosophy = CreatePhilosophy.Execute(_cosmos, _person);
                JoinPhilosophy.Execute(_cosmos, _person, newPhilosophy, false);
            }
            Time = null;
            IsFixed = true;
        }
    }
}
