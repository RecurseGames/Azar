﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recurse.Azar.Model.Philosophies
{
    public static class CreatePhilosophy
    {
        private static readonly SyllableList EndSyllables = new SyllableList();

        static CreatePhilosophy()
        {
            EndSyllables.AddRange(Syllable.CreateEnd(
                SyllableEnd.Vowel,
                new[] { SyllableEnd.Consonant },
                "ism"));
            EndSyllables.AddRange(Syllable.CreateEnd(
                SyllableEnd.Consonant,
                new[] { SyllableEnd.Vowel },
                "wism", "tism", "psism", "phism", "fism", "dism", "gism", "kism", "mism", "nism", "bism", "vism"));
        }

        public static Philosophy Execute(Cosmos cosmos, Person founder)
        {
            var existingPhilosophy = HasPhilosophy.Check(founder);

            var philosophy = new Philosophy(
                founder.Location.Get(),
                CreateName(cosmos.Rng, founder))
            {
                Parent = existingPhilosophy,
                DefiningTrait = cosmos.Rng.Select(Trait.List),
                DefiningTraitValue = Math.Pow(cosmos.Rng.NextDouble(), 2) * (cosmos.Rng.NextBool() ? -1 : 1)
            };

            var record = new ActionRecord(cosmos.CreateInstant(), Singleton<FoundVerb>.Instance)
            {
                Subjects = { founder },
                Objects = { philosophy }
            };
            founder.Attributes.Add(new HasFoundedPhilosophy(philosophy));
            founder.Records.Add(record);
            philosophy.Records.Add(record);
            philosophy.Origin.Records.Add(record);
            
            return philosophy;
        }

        private static string CreateName(Random rng, Person founder)
        {
            Syllable[] name = null;
            for (var i = 0; i < 1000 && name == null; i++)
                name = TryCreateName(rng, founder);
            if (name == null) throw new InvalidOperationException("Failed to generate name.");
            return string.Join("", name.Select(s => s.Text).ToArray());
        }

        private static Syllable[] TryCreateName(Random rng, Person founder)
        {
            var nameLength = rng.Next(3, founder.NameSyllables.Length);
            var name = founder.NameSyllables.Take(nameLength - 1).ToList();
            if (EndSyllables.TryAdd(rng, name, true))
                return name.ToArray();
            return null;
        }
    }
}
