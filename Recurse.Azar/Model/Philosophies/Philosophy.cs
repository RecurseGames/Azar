﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;
using Recurse.Common.Maths.Hashing;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Personalities;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Politics;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Philosophies
{
    public class Philosophy : Entity
    {
        private readonly string _name;

        public readonly Location Origin;

        private readonly Dictionary<Philosophy, double> _conversionRates =
            new Dictionary<Philosophy, double>();

        private readonly double
            // Relative ability (from 0 to 1) to gain new followersw with no previous philosophy
            _spreadRate,
            // Relative ability (from 0 to 1) to gain followers outside birthplace
            _range,
            // Relative ability (from 0 to 1) to gain new followers from other philosophies
            _conversionRate;

        public readonly List<Person> Adherents = new List<Person>();

        public Philosophy Parent { get; set; }

        public string Adjective
        {
            get { return _name.Substring(0, _name.Length - 1) + 't'; }
        }

        public Trait DefiningTrait
        {
            get; set;
        }

        public double DefiningTraitValue
        {
            get; set;
        }

        public Philosophy(Location origin, string name)
        {
            Origin = origin;
            _name = StringUtils.TitleCase(name);

            _spreadRate = 1 - Math.Pow(MurmurHash.Get(0x60625af0, this).ToFloat(), 3);
            _range = MurmurHash.Get(0x76ed649f, this).ToFloat();
            _conversionRate = MurmurHash.Get(0xf685924e, this).ToFloat();
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            return Adherents.Where(GetIsImportant.Execute).Cast<Entity>();
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            yield return RecordType.Conversion;
            yield return RecordType.Political;
            yield return RecordType.Death;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(aliases, _name ?? "an unnamed religion", Pronoun.It, this);
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield return new CategoryRecord(this, Parent != null ?
                "branch of" + Parent._name :
                "religion");
            if (DefiningTrait != null)
            {
                yield return new SimpleRecord(
                    "it " + (DefiningTraitValue < .5 ? DefiningTrait.LowerDescription : DefiningTrait.HighDescription));
            }
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public double GetConversionChance(Person person)
        {
            var philosophy = HasPhilosophy.Check(person);
            var baseChance = philosophy != null ?
                GetConversionRate(philosophy) :
                _spreadRate;

            var location = person.Location.Get();
            if (Origin.Neighbours.Contains(location))
                baseChance *= _spreadRate;
            else if (!Equals(location, Origin))
                baseChance *= Math.Pow(_spreadRate, 2);

            return baseChance / 2;
        }

        private double GetConversionRate(Philosophy other)
        {
            if (!_conversionRates.ContainsKey(other))
            {
                _conversionRates[other] = Personality.Merge(
                    Personality.Merge(_conversionRate, 1 - other._conversionRate),
                    MurmurHash.Get(0xb04b67d8, this, other).ToFloat());
            }
            return _conversionRates[other];
        }
    }
}
