﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Philosophies;
using RG.Saga.Model.Timing.Randomised;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Occupations
{
    /// <summary>
    /// This event has people attempt to convert others to their philosophy
    /// </summary>
    public class RecruitEvent : MutableEvent<Time<Duration>?>
    {
        private readonly Person _person;

        private readonly Cosmos _cosmos;

        public RecruitEvent(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;

            UpdateTime();
        }

        public override void Occur()
        {
            var philosophy = HasPhilosophy.Check(_person);
            if (philosophy != null)
            {
                var candidates = _person.Location.Get().Contents
                    .OfType<Person>()
                    .Where(p => !p.IsDead)
                    .Where(p => !p.Attributes.OfType<HasPhilosophy>().Any(a => Equals(a.Philosophy, philosophy)))
                    .ToList();
                if (candidates.Any())
                {
                    var target = _cosmos.Rng.Select(candidates);
                    if (_cosmos.Rng.NextDouble() < philosophy.GetConversionChance(target))
                    {
                        JoinPhilosophy.Execute(_cosmos, target, philosophy, true);
                    }
                }
            }
            UpdateTime();
        }

        private void UpdateTime()
        {
            var hasOccupation = HasOccupation.Get(_person);
            var occupation = hasOccupation != null ? hasOccupation.Occupation : null;
            
            var annualChance = new[] { Occupation.Mystic, Occupation.Poet }.Contains(occupation) ?
                .5 :
                .2;
            Time = _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, annualChance) * Durations.Year;
        }
    }
}
