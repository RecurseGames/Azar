﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Personalities;
using System.Linq;

namespace Recurse.Azar.Model.Philosophies
{
    public class HasPhilosophy : IAffectsTrait, IAttribute
    {
        public Philosophy Philosophy { get; set; }

        public double DefiningTraitValue { get { return Philosophy.DefiningTraitValue; } }

        public Trait DefiningTrait { get { return Philosophy.DefiningTrait; } }

        public HasPhilosophy(Philosophy philosophy)
        {
            Philosophy = philosophy;
        }

        public static Philosophy Check(Entity entity)
        {
            var attribute = entity.Attributes.OfType<HasPhilosophy>().SingleOrDefault();
            return attribute != null ? attribute.Philosophy : null;
        }
    }
}
