﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recurse.Azar.Model.Philosophies
{
    public static class JoinPhilosophy
    {
        public static void Execute(
            Cosmos cosmos, 
            Person person,
            Philosophy philosophy,
            bool record)
        {
            var hasPhilosophy = person.Attributes.OfType<HasPhilosophy>().SingleOrDefault();
            if (hasPhilosophy == null)
            {
                hasPhilosophy = new HasPhilosophy(philosophy);
                person.Attributes.Add(hasPhilosophy);
            }
            else
            {
                hasPhilosophy.Philosophy.Adherents.Remove(person);
                hasPhilosophy.Philosophy = philosophy;
            }
            philosophy.Adherents.Add(person);

            if (record)
            {
                var conversionRecord = new ActionRecord(cosmos.CreateInstant(), Singleton<ConvertToVerb>.Instance)
                {
                    Subjects = { person },
                    Objects = { philosophy },
                    Type = RecordType.Conversion
                };
                person.Records.Add(conversionRecord);
            }
        }
    }
}
