﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.People.Attributes;
using System;
using System.Linq;

namespace Recurse.Azar.Model.Sex
{
    public class Sexuality
    {
        public static readonly Sexuality Default = new Sexuality(
            Gender.NonBinary,
            Pronoun.Neutral,
            ReproductiveAbility.None,
            new Gender[] { Gender.Male, Gender.Female, Gender.NonBinary });
        
        public static readonly WeightedOutcomes<Pronoun>
            Neutral = new WeightedOutcomes<Pronoun>
                {
                    { Pronoun.Neutral, 5 },
                    { Pronoun.Spivak, 1 },
                    { Pronoun.Hir, 1 },
                    { Pronoun.Zir, 1 },
                    { Pronoun.Ne, .5 },
                    { Pronoun.Ve, .5 },
                    { Pronoun.Xe, 1 }
                };

        /// <summary>
        /// The gender used to determine who is attracted to this
        /// person. It may loosely correlates with biological sex and
        /// gender, but is not the same. This avoids making assumptions
        /// about what, exactly, is the source of legitimate sexual
        /// orientation.
        /// </summary>
        private readonly Gender _type;

        /// <summary>
        /// The types (see <see cref="_type"/>) of Sexuality this person is attracted to.
        /// </summary>
        private readonly Gender[] _attractedTo;

        public readonly Pronoun Pronouns;

        public readonly ReproductiveAbility Reproduction;

        public string SpouseNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "husband";
                    case GrammarGender.Female: return "wife";
                    default: return "spouse";
                }
            }
        }

        public string ParentNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "father";
                    case GrammarGender.Female: return "mother";
                    default: return "parent";
                }
            }
        }

        public string UncleNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "uncle";
                    case GrammarGender.Female: return "aunt";
                    default: return "parent's sibling";
                }
            }
        }

        public string AdultNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "a man";
                    case GrammarGender.Female: return "a woman";
                    default: return "an enby";
                }
            }
        }

        public string MonarchTitle
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "King";
                    case GrammarGender.Female: return "Queen";
                    default: return "Monarch";
                }
            }
        }

        public string LordTitle
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "Lord";
                    case GrammarGender.Female: return "Lady";
                    default: return "Laird";
                }
            }
        }

        public string JuvenileNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "a boy";
                    case GrammarGender.Female: return "a girl";
                    default: return "an enby";
                }
            }
        }

        public string ChildNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "son";
                    case GrammarGender.Female: return "daughter";
                    default: return "child";
                }
            }
        }

        public string SiblingNoun
        {
            get
            {
                switch (Pronouns.Gender)
                {
                    case GrammarGender.Male: return "brother";
                    case GrammarGender.Female: return "sister";
                    default: return "sibling";
                }
            }
        }

        public Sexuality(
            Gender type,
            Pronoun pronouns,
            ReproductiveAbility reproduction,
            Gender[] attractedTo)
        {
            _type = type;
            _attractedTo = attractedTo;
            Pronouns = pronouns;
            Reproduction = reproduction;
        }

        public Sexuality(Random rng) : this(rng, RandomTypes.GetOutcome(rng))
        {

        }

        private Sexuality(Random rng, Gender type) : this(
            type,
            GetTypicalPronoun(rng, Gender.Mutate(rng, type)),
            ReproductiveAbility.GetTypical(Gender.Mutate(rng, type)),
            GetAttraction(rng, type))
        {

        }

        public bool IsAttractedTo(Sexuality other)
        {
            return _attractedTo.Contains(other._type);
        }

        private static Pronoun GetTypicalPronoun(Random rng, Gender gender)
        {
            if (Equals(gender, Gender.Male)) return Pronoun.Male;
            if (Equals(gender, Gender.Female)) return Pronoun.Female;
            return Neutral.GetOutcome(rng);
        }

        private static Gender[] GetAttraction(Random rng, Gender type)
        {
            // Some people will not have any romantic attachments.
            if (rng.NextDouble() < .01) return new Gender[0];

            // Most people will be attracted to either Male or female 'types',
            // to ensure reasonable birth rates. Note that 'type' abstracts away
            // biological sex and gender (its relationship to them is ambiguous,
            // although they loosely correlate), allowing us to avoid deciding
            // what is and isn't a legitimate sexual orientation entirely.
            if (rng.NextDouble() < .8)
                return new[] { GetSexualPreference(rng, type), Gender.NonBinary };

            // Yay pansexuals! Various genitals! Sexy sex!
            return new[] { Gender.Male, Gender.Female, Gender.NonBinary };
        }

        private static Gender GetSexualPreference(Random rng, Gender type)
        {
            if (Equals(type, Gender.Male))
                return rng.NextDouble() < .1 ? Gender.Male : Gender.Female;
            if (Equals(type, Gender.Female))
                return rng.NextDouble() < .1 ? Gender.Female : Gender.Male;
            return rng.NextDouble() < .5 ? Gender.Female : Gender.Male;
        }

        private static readonly WeightedOutcomes<Gender> RandomTypes = new WeightedOutcomes<Gender>
        {
            { Gender.Male, 19 },
            { Gender.Female, 19 },
            { Gender.NonBinary, 2 }
        };
    }
}
