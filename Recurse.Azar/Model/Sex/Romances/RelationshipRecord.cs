﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using System.Collections.Generic;
using System;
using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Sex
{
    public class RelationshipRecord : IRecord
    {
        private readonly ICollection<CausalRelationship> _causalRelationships = 
            new List<CausalRelationship>();

        private readonly List<IRecord> _errata = new List<IRecord>();

        private readonly Time<Duration> _time;

        private readonly Romance _romance;

        public IEnumerable<Entity> Mentions
        {
            get { return _romance.Members.OfType<Entity>(); }
        }

        public RecordType Type { get { return 0; } }

        IEnumerable<IRecord> IRecord.Errata
        {
            get { return _errata; }
        }

        public List<IRecord> Errata
        {
            get { return _errata; }
        }

        public Location Location
        {
            get { return Entity.GetLocation(_romance.Members, _time); }
        }

        public Time<Duration> Time
        {
            get { return _time; }
        }

        Time<Duration>? IRecord.Time
        {
            get { return _time; }
        }

        public ICollection<CausalRelationship> CausalRelationships
        {
            get { return _causalRelationships; }
        }

        public Hyperstring GetPrimaryClause(TextContext context, GrammarCategory category)
        {
            context.PrependScene = true;

            var focus =
                _romance.GetPartnerOrNull(context.Focus as Person) ??
                _romance.GetPartnerOrNull(context.Aliases.Targets.OfType<Person>().Intersect(_romance.Members).FirstOrDefault()) ??
                _romance.Initiator;
            var topic = _romance.GetPartnerOrNull(focus);

            var result = Hyperstring.Empty;

            var focusAge = (int) Math.Floor(Age.GetYears(topic, _time));
            if (!context.NotedAges.ContainsKey(focus) || context.NotedAges[focus] != focusAge)
            {
                result +=
                    Hyperstring.Empty +
                    "at the age of" +
                    focusAge.ToString() +
                    RenderSymbol.Comma;
                context.NotedAges[focus] = focusAge;
            }

            result += Singleton<BeginRomanceWithVerb>.Instance.ToSequence(
                topic.GetReference(context),
                focus.GetReference(context),
                GrammarCategory.SimplePast);
            
            var topicAge = (int)Math.Floor(Age.GetYears(focus, _time));
            if (!context.NotedAges.ContainsKey(topic) || context.NotedAges[topic] != topicAge)
            {
                result +=
                    Hyperstring.Empty +
                    RenderSymbol.Comma +
                    "who was" +
                    topicAge.ToString();
                context.NotedAges[topic] = topicAge;
            }
            
            context.Aliases.Add(Pronoun.Neutral, Entity.GetReference(_romance.Members, context));
            return result;
        }

        public RelationshipRecord(Time<Duration> time, Romance romance)
        {
            _time = time;
            _romance = romance;
        }
    }
}
