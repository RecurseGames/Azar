﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;

namespace Recurse.Azar.Language.Verbs
{
    public class BeginRomanceWithVerb : SimpleGrammarAction
    {
        public override bool? RequiredTransitivity
        {
            get { return true; }
        }

        protected override Hyperstring GetVerb(GrammarCategory category, bool isTransitive)
        {
            switch (category)
            {
                case GrammarCategory.SimplePast:
                    return "began a relationship with";
                case GrammarCategory.SimplePastFuture:
                    return "would begin a relationship with";
                default:
                    return null;
            }
        }
    }
}
