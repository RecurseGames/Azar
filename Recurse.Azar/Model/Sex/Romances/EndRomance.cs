﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Sex.Romances
{
    public static class EndRomance
    {
        public static void Execute(
            Cosmos cosmos,
            Romance romance,
            ActionRecord cause = null,
            IEnumerable<Person> responsibleParties = null)
        {
            var record = new ActionRecord(cosmos.CreateInstant(), Singleton<EndVerb>.Instance)
            {
                Subjects = { romance }
            };
            romance.Start.Errata.Add(record);
            romance.End = cosmos.Time;
            romance.Records.Add(record);

            if (cause != null)
                new CausalRelationship(cause, record).Apply();
            
            if (responsibleParties != null)
            {
                var blamableParties = responsibleParties.ToList();
                foreach (var victim in romance.Members.Except(blamableParties))
                {
                    Personality.TestJealousy(
                        cosmos,
                        victim,
                        cosmos.Rng.Select(blamableParties),
                        cause);
                }
            }

            foreach (var member in romance.Members)
            {
                var attributesToRemove = member.Attributes
                    .OfType<HasRomance>()
                    .Where(r => Equals(r.Romance, romance))
                    .ToArray();
                foreach (var attributeToRemove in attributesToRemove)
                    member.Attributes.Remove(attributeToRemove);
            }

            romance.Events.Cancel();
        }
    }
}
