﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Lifecycles;
using System;
using RG.Saga.Model.Timing.Randomised;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Sex.Reproduction;
using System.Collections.Generic;
using System.Linq;
using Recurse.Common.Extensions;
using Recurse.Azar.Model.Personalities;

namespace Recurse.Azar.Model.People.Records
{
    public class RomanceStarting : Stage
    {
        private readonly Destiny _destiny;

        private readonly Romance _romance;

        private readonly Cosmos _cosmos;

        public RomanceStarting(Cosmos cosmos, Romance romance)
        {
            _romance = romance;
            _cosmos = cosmos;
            
            if (_cosmos.Rng.NextDouble() < -.3 + Trait.Disagreeableness.Get(_romance.Initiator, _romance.Other))
            {
                _destiny = Destiny.End;
            }
            else if (_cosmos.Rng.NextDouble() < .3 + Trait.MarriageChance.Get(_romance.Initiator, _romance.Other))
            {
                _destiny = Destiny.Officiate;
            }
        }

        protected override void OnStart()
        {
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            // Romances tend to end or officiate earlier, so we weight annual chances upwards

            switch (_destiny)
            {
                case Destiny.End:
                    {
                        var chance = 1 - Math.Pow(Trait.BreakUpSpeed.Get(_romance.Initiator, _romance.Other), 2);

                        var time = _cosmos.Time;
                        while (time.CompareTo(_romance.Start.Time + Durations.Year) < 0)
                            time += GeometricDistribution.GetNext(_cosmos.Rng, chance) * Durations.Year;

                        yield return new Occurence(
                            () =>
                            {
                                EndRomance.Execute(
                                    _cosmos,
                                    _romance,
                                    null,
                                    new[] { _cosmos.Rng.Select(_romance.Members.ToArray()) });
                                Transition(null);
                            },
                            time);
                    }
                    break;
                case Destiny.Officiate:
                    {
                        var chance = 1 - Math.Pow(Trait.MarriageSpeed.Get(_romance.Initiator, _romance.Other), 2);

                        var time = _cosmos.Time;
                        while (time.CompareTo(_romance.Start.Time + Durations.Year) < 0)
                            time += GeometricDistribution.GetNext(_cosmos.Rng, chance) * Durations.Year;

                        yield return CreateTransition(new RomanceOfficiating(_cosmos, _romance), time);
                    }
                    break;
            }
        }

        private enum Destiny
        {
            None, End, Officiate
        }
    }
}
