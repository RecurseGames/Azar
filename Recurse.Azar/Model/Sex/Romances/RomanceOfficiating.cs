﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Lifecycles;
using System;
using Recurse.Azar.Model.Locations;
using Recurse.Ianna.Model;
using System.Linq;
using Recurse.Azar.Model.Records;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Sex.Romances;
using System.Collections.Generic;
using Recurse.Common.Extensions;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Personalities;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People.Records
{
    public class RomanceOfficiating : Stage
    {
        private readonly Cosmos _cosmos;

        private readonly Romance _romance;

        private Time<Duration>? _expiryDate;

        public RomanceOfficiating(Cosmos cosmos, Romance romance)
        {
            _cosmos = cosmos;
            _romance = romance;
        }

        protected override void OnStart()
        {
            if (_romance.IsOfficial)
                throw new InvalidOperationException("Romance is already officiated.");
            _romance.IsOfficial = true;

            var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<IsMarriedVerb>.Instance)
            {
                Subjects = { _romance.Initiator, _romance.Other },
                Type = RecordType.Marriage
            };
            _romance.Initiator.Records.Add(record);
            _romance.Other.Records.Add(record);

            // An official romance may end other romances
            foreach (var member in _romance.Members)
            {
                foreach (var otherRomance in member.Attributes
                    .OfType<HasRomance>()
                    .Select(r => r.Romance)
                    .Where(r => !r.IsOfficial)
                    .ToArray())
                {
                    if (!Equals(_romance, otherRomance) &&
                        _cosmos.Rng.NextDouble() < _romance.GetStopOtherChanceWhenOfficial())
                    {
                        EndRomance.Execute(
                            _cosmos,
                            otherRomance,
                            record,
                            _romance.Members);
                    }
                }
            }

            // An official romance will last for a (high) random number of years,
            // generally long enough to ensure they last until death.
            // We can calculate divorce rates later.
            _expiryDate = 
                _cosmos.Time + 
                _cosmos.Rng.NextDouble() * 150 * Trait.DivorceChance.Get(_romance.Initiator, _romance.Other) * Durations.Year;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            if (_expiryDate.HasValue)
            {
                yield return new Occurence(
                    () =>
                    {
                        Transition(null);

                        var instigator = _cosmos.Rng.Select(_romance.Members.ToArray());
                        var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<DivorceVerb>.Instance)
                        {
                            Subjects = { instigator },
                            Objects = { _romance.GetPartnerOrNull(instigator) }
                        };

                        EndRomance.Execute(
                            _cosmos,
                            _romance,
                            record,
                            new[] { instigator });
                    },
                    _expiryDate.Value);
            }
        }
    }
}
