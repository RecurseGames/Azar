﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class HasRomance : IAttribute
    {
        public readonly Romance Romance;

        public readonly Person Partner;

        public HasRomance(Romance romance, Person partner)
        {
            Partner = partner;
            Romance = romance;
        }
    }
}
