﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Maths.Interpolations;
using Recurse.Common.Utils;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Sex.Romances;
using RG.Saga.Model.Timing.Randomised;
using System.Linq;

namespace Recurse.Azar.Model.People.Events
{
    public class RomancesUpdateEvent : PersonEvent
    {
        private readonly LinearInterpolation AgeToActivityRate = new LinearInterpolation
        {
            { 18, .6 }
        };

        public RomancesUpdateEvent(Cosmos cosmos, Person person) : base(cosmos, person)
        {
            Occur();
        }

        public override void Occur()
        {
            var age = Age.GetYears(Person, Cosmos.Time);
            if (age < 18)
            {
                Time = Cosmos.Time + (18 - age) * Durations.Year;
            }
            else
            {
                OnChange();
            }
            Time += GeometricDistribution.GetNext(Cosmos.Rng, AgeToActivityRate.Get(age)) * Durations.Year;
        }

        private void OnChange()
        {
            var location = Person.Location.Get();
            if (location != null)
            {
                // Seek new relationship...
                var candidate = Cosmos.Rng.Select(location.Contents.OfType<Person>().ToArray());
                if (Romance.IsDesired(Cosmos, Cosmos.Rng, Person, candidate))
                {
                    if (Romance.IsDesired(Cosmos, Cosmos.Rng, candidate, Person))
                    {
                        StartRomance.Execute(Cosmos, Person, candidate);
                        return;
                    }

                    // We've been rejected. Some people might react with anger.
                    // Those people are fucking creeps.
                    var rejection = new ActionRecord(Cosmos.CreateInstant(), Singleton<Rejected>.Instance)
                    {
                        Subjects = { candidate },
                        Objects = { Person }
                    };
                    Personality.TestJealousy(
                        Cosmos,
                        Person,
                        candidate,
                        rejection);
                }
            }
        }
    }
}
