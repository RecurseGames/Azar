﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Reproduction;

namespace Recurse.Azar.Model.Sex.Romances
{
    public static class StartRomance
    {
        public static void Execute(Cosmos cosmos, Person initiator, Person target)
        {
            var romance = new Romance(initiator, target);
            cosmos.Events.TryAdd(romance.Events);

            var record = new RelationshipRecord(cosmos.Time, romance);
            romance.Start = record;
            romance.Records.Add(record);
            initiator.Records.Add(record);
            target.Records.Add(record);

            initiator.Attributes.Add(new HasRomance(romance, target));
            target.Attributes.Add(new HasRomance(romance, initiator));
            
            romance.Events.TryAdd(new ConsiderParenthood(cosmos, romance));
            romance.Events.TryAdd(new LifecycleEvent { Stage = new RomanceStarting(cosmos, romance) });
        }
    }
}
