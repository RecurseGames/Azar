﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Hashing;
using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Model.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Sex
{
    public class Romance : Entity
    {
        public readonly Person Initiator, Other;

        public RelationshipRecord Start { get; set; }

        public bool IsOfficial { get; set; }

        public IEnumerable<Person> Members
        {
            get
            {
                yield return Initiator;
                yield return Other;
            }
        }

        public Time<Duration>? End
        {
            get; set;
        }

        public Romance(Person initiator, Person other)
        {
            Initiator = initiator;
            Other = other;

            Records.Handlers.Add(Initiator.Records.Add);
            Records.Handlers.Add(Other.Records.Add);
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            var alias = aliases.Get(this);
            if (alias != null) return alias;
            return new OwnedGrammarObject(
                GetSimpleReference(Members, aliases),
                "relationship");
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield break;
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            throw new InvalidOperationException();
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            yield break;
        }

        /// <summary>
        /// Gets the chance that a romance will end (or prevent) other
        /// informal romances upon officiation
        /// </summary>
        public double GetStopOtherChanceWhenOfficial()
        {
            return Trait.Chastity.Get(Initiator, Other) + .5;
        }

        public Person GetPartnerOrNull(Person member)
        {
            if (Equals(Initiator, member)) return Other;
            if (Equals(Other, member)) return Initiator;
            return null;
        }

        public static bool IsDesired(Cosmos cosmos, Random rng, Person initiator, Person other)
        {
            // Prevent necrophilia
            if (other.IsDead)
                return false;

            // Prevent narcissism
            if (Equals(initiator, other))
                return false;

            // Prevent incest
            if (ParentageRelationship.GetSelfAndParentsAndGrandparents(initiator)
                .Intersect(ParentageRelationship.GetSelfAndParentsAndGrandparents(other))
                .Any())
                return false;

            // Prevent multiple relationships with same person
            if (initiator.Attributes.OfType<HasRomance>().Any(r => Equals(r.Partner, other)))
                return false;

            // Prevent pedophilia
            var initiatorAge = Age.GetYears(initiator, cosmos.Time);
            var otherAge = Age.GetYears(other, cosmos.Time);
            if (otherAge < 18 || initiatorAge < 18)
                return false;

            // Age difference makes attraction less likely
            var ageDifference = initiatorAge > otherAge ? 
                (initiatorAge / otherAge) :
                (otherAge / initiatorAge);
            if (rng.NextDouble() > 1d / ageDifference)
                return false;

            // Respect sexuality
            if (!initiator.Sexuality.IsAttractedTo(other.Sexuality))
                return false;

            // Avoid relationships while married
            var avoidBigamyChance = initiator.Attributes
                .OfType<HasRomance>()
                .Select(r => r.Romance)
                .Where(r => r.IsOfficial)
                .Select(r => r.GetStopOtherChanceWhenOfficial())
                .DefaultIfEmpty()
                .Max();
            if (rng.NextDouble() < avoidBigamyChance)
            {
                return false;
            }

            // Avoid too many relationships
            if (rng.NextDouble() > Math.Pow(
                Trait.FaithWithinMarriage.Get(initiator, other) * .5 + .5,
                initiator.Attributes.OfType<HasRomance>().Count()))
            {
                return false;
            }

            return true;
        }

        public override string ToString()
        {
            return "Romance(" + string.Join(", ", Members.Select(m => m.ToString()).ToArray()) + ")";
        }
    }
}
