﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using System;

namespace Recurse.Azar.Model.People.Attributes
{
    public class Gender
    {
        /// <summary>
        /// Rough percentage of people who are genderqueer/trans/etc
        /// </summary>
        public const double Queerness = .1;

        public Hyperstring ChildNoun { get; set; }

        public Hypersymbol SpouseNoun { get; set; }

        public static readonly Gender
            Male = new Gender
            {
                ChildNoun = "son",
                SpouseNoun = "husband"
            },
            Female = new Gender
            {
                ChildNoun = "daughter",
                SpouseNoun = "wife"
            },
            NonBinary = new Gender
            {
                ChildNoun = "child",
                SpouseNoun = "spouse"
            };
        
        public static Gender Mutate(Random rng, Gender gender)
        {
            if (rng.NextDouble() > .1) return gender;
            if (Equals(gender, Male))
                return rng.NextDouble() < .5 ? Female : NonBinary;
            if (Equals(gender, Female))
                return rng.NextDouble() < .5 ? Male : NonBinary;
            if (Equals(gender, NonBinary))
                return rng.NextDouble() < .5 ? Female : Male;
            throw new ArgumentException("Gender not recognised");
        }
    }
}
