﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People.Attributes;
using System;

namespace Recurse.Azar.Model.Sex
{
    /// <summary>
    /// Represents a person's sexual orientation. No sexual orientation
    /// indicates aSex.
    /// </summary>
    public class SexualOrientation : Attribute
    {
        public static readonly SexualOrientation
            // Every sexual person is considered pansexual. This includes
            // gray asexuals, as we're only interested in the presence of
            // sexual interest, not its degree.
            // Including heteroSex, homoSex, and biSex
            // introduce issues of transphobia too complex to list here.
            Pansexual = new SexualOrientation();
    }
}
