﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Residency;
using System;
using System.Linq;
using System.Collections.Generic;
using Recurse.Azar.Model.Sex.Reproduction;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class PregnancyEnd : Stage
    {
        private readonly Person _mother, _father;

        private readonly Cosmos _cosmos;

        public PregnancyEnd(Cosmos cosmos, Person mother, Person father = null)
        {
            _cosmos = cosmos;
            _mother = mother;
            _father = father;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield break;
        }

        protected override void OnStart()
        {
            var child = CreatePerson.Execute(_cosmos);
            CauseBirth.Execute(_cosmos, child, _mother, _father);
            AddToBloodline.Execute(_cosmos, child, _mother, _father);
            ActivatePerson.Execute(_cosmos, child);
        }
    }
}
