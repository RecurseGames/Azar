﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Hashing;
using Recurse.Common.Scheduling.Events;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Sex.Reproduction
{
    public class ConsiderParenthood : MutableEvent<Time<Duration>?>
    {
        private readonly Cosmos _cosmos;

        private readonly Romance _romance;

        public ConsiderParenthood(Cosmos cosmos, Romance romance)
        {
            _cosmos = cosmos;
            _romance = romance;

            Time = cosmos.Time;
        }

        public override void Occur()
        {
            // Ensure colocation
            var location = Entity.GetLocation(_romance.Members);
            if (location != null)
            {
                var desire = _romance.Members.Select(GetDesiredExtraChildren).Average();
                var pregnancyPossibility = PregnancyStart.GetPossibility(_cosmos, _romance.Members.ToArray());
                if (desire > 0 || pregnancyPossibility > 0 && _cosmos.Rng.NextDouble() < .005)
                {
                    if (pregnancyPossibility <= 0)
                    {
                        if (_cosmos.Rng.NextDouble() < .5)
                        {
                            var child = CreatePerson.Execute(_cosmos);
                            var childBirth = CauseBirth.Execute(_cosmos, child, location);
                            AddToBloodline.Execute(_cosmos, child, _romance.Members.ToArray());
                            ActivatePerson.Execute(_cosmos, child);
                            
                            var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<AdoptVerb>.Instance)
                            {
                                Subjects = { _romance.Initiator, _romance.Other },
                                Objects = { child }
                            };
                            _romance.Initiator.Records.Add(record);
                            _romance.Other.Records.Add(record);
                            childBirth.Start.Errata.Add(record);
                            
                            new ParentageRelationship(_romance.Initiator, child, false).Apply();
                            new ParentageRelationship(_romance.Other, child, false).Apply();
                        }
                    }
                    else if (_cosmos.Rng.NextDouble() < pregnancyPossibility)
                    {
                        Person mother, father;
                        PregnancyStart.CategoriseParents(out mother, out father, _romance.Members.ToArray());
                        mother.Events.TryAdd(new LifecycleEvent { Stage = new PregnancyStart(_cosmos, mother, father) });
                    }
                }
            }
            Time += 9 * Durations.Month;
        }

        public static int GetDesiredChildren(Person person)
        {
            var coparentCount = person.Attributes
                .OfType<HasRomance>()
                .Select(r => r.Romance)
                .Select(r => r.IsOfficial ? 1 : Trait.CommunalParenthood.Get(person))
                .DefaultIfEmpty()
                .Sum();
            var innateDesire = Trait.ChildDesire.Get(person) * 5;
            return (int) Math.Round(innateDesire * coparentCount);
        }

        public static int GetDesiredExtraChildren(Person person)
        {
            return GetDesiredChildren(person) - ParentageRelationship.GetChildren(person).Count();
        }
    }
}
