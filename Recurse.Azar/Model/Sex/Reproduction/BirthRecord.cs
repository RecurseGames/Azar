﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Text;
using System.Collections.Generic;
using System;
using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using System.Linq;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Language.Objects;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Sex
{
    public class BirthRecord : IRecord
    {
        private readonly ICollection<CausalRelationship> _causalRelationships = 
            new List<CausalRelationship>();

        private readonly List<IRecord> _errata = new List<IRecord>();

        private readonly Time<Duration> _time;

        private readonly Person _child, _mother, _father;

        private readonly Location _location;

        public RecordType Type { get { return RecordType.Birth; } }

        public IEnumerable<Entity> Mentions
        {
            get { return new Entity[] { _child, _mother, _father }.Where(p => p != null); }
        }

        IEnumerable<IRecord> IRecord.Errata
        {
            get { return _errata; }
        }

        public List<IRecord> Errata
        {
            get { return _errata; }
        }

        public Location Location
        {
            get { return _location; }
        }

        public Time<Duration>? Time
        {
            get { return _time; }
        }

        public ICollection<CausalRelationship> CausalRelationships
        {
            get { return _causalRelationships; }
        }

        public BirthRecord(Time<Duration> time, Location location, Person child, Person mother, Person father)
        {
            _time = time;
            _location = location;
            _child = child;
            _father = father;
            _mother = mother;
        }
        
        public Hyperstring GetPrimaryClause(TextContext context, GrammarCategory category)
        {
            if (!Equals(context.Focus, _child) && _mother != null)
            {
                var oldFocus = context.Focus;
                context.Focus = _father ?? oldFocus;
                try
                {
                    return Singleton<GiveBirth>.Instance.ToSequence(
                        _mother.GetReference(context),
                        _child.GetReference(context),
                        GrammarCategory.SimplePast);
                }
                finally
                {
                    context.Focus = oldFocus;
                }
            }
            else
            {
                return Singleton<IsBornVerb>.Instance.ToSequence(
                    _child.GetReference(context),
                    null,
                    GrammarCategory.SimplePast);
            }
        }
    }
}
