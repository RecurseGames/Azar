﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Locations;
using System.Linq;
using Recurse.Common.Text;
using Recurse.Azar.Model.Attributes;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Records;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Sex;

namespace Recurse.Azar.Model.People.Records
{
    /// <summary>
    /// Records a person's birth
    /// </summary>
    public class Birth
    {
        public BirthRecord Start { get; set; }

        //public override GrammarNoun GetRecordEventNoun(AliasMapping pronouns, PronounType type)
        //{
        //    var result =
        //        Hyperstring.Empty +
        //        _child.GetNoun(pronouns, PronounType.Their).Hyperstring +
        //        "birth";
        //    return result;
        //}
    }
}
