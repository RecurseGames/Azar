﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People.Attributes;
using System;

namespace Recurse.Azar.Model.Sex
{
    public class ReproductiveAbility : Attribute
    {
        public static readonly ReproductiveAbility
            CanGetPregnant = new ReproductiveAbility(),
            CanImpregnate = new ReproductiveAbility(),
            None = new ReproductiveAbility();

        public static ReproductiveAbility GetTypical(Gender gender)
        {
            if (Equals(gender, Gender.Male)) return CanImpregnate;
            if (Equals(gender, Gender.Female)) return CanGetPregnant;
            return None;
        }
    }
}
