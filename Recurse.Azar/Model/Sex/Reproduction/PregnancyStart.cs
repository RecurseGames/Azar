﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Maths.Interpolations;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using System;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Sex.Romances
{
    public class PregnancyStart : Stage
    {
        private const double PeakFertilityRate = .5;

        private static readonly LinearInterpolation FemaleFertility = new LinearInterpolation
        {
            { 16, 0 },
            { 18, PeakFertilityRate },
            { 35, PeakFertilityRate },
            { 40, 0 }
        };

        private Time<Duration>? _start;

        private readonly PregnancyEnd _end;

        private readonly Cosmos _cosmos;

        public static void CategoriseParents(out Person mother, out Person father, params Person[] parents)
        {
            if (parents == null || parents.Length != 2)
                throw new ArgumentException("Pregnancy needs 2 parents");
            mother = parents.SingleIfAny(
                p => Equals(p.Sexuality.Reproduction, ReproductiveAbility.CanGetPregnant));
            father = parents.SingleIfAny(
                p => Equals(p.Sexuality.Reproduction, ReproductiveAbility.CanImpregnate));
        }

        public static double GetPossibility(Cosmos cosmos, params Person[] parents)
        {
            Person mother, father;
            CategoriseParents(out mother, out father, parents);
            if (mother == null || father == null) return 0;
            if (Age.GetYears(father, cosmos.Time) < 18) return 0;
            return FemaleFertility.Get(Age.GetYears(mother, cosmos.Time));
        }

        public PregnancyStart(Cosmos cosmos, Person mother, Person father = null)
        {
            _cosmos = cosmos;
            _end = new PregnancyEnd(cosmos, mother, father);
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return CreateTransition(
                _end,
                _start.Value + 9 * Durations.Month + (_cosmos.Rng.NextDouble() - .5) * Durations.Month);
        }

        protected override void OnStart()
        {
            _start = _cosmos.Time;
        }
    }
}
