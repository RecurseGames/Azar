﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class StartLordship
    {
        public static bool TryExecute(
            Cosmos cosmos,
            Person person,
            Location location,
            IRecord cause)
        {
            if (person == null)
            {
                Execute(cosmos, person, location);
                return true;
            }
            if (person.Attributes.OfType<HasBeenLord>().Count(a => a.IsActive) > 2)
            {
                return false;
            }

            var hasBeenLord = Execute(cosmos, person, location);

            var record = new ActionRecord(cosmos.CreateInstant(), Singleton<BecomeVerb>.Instance)
            {
                Subjects = { person },
                Objects = { hasBeenLord.Title },
                Type = RecordType.Political
            };
            person.Records.Add(record);
            location.Records.Add(record);
            if (cause != null)
                new CausalRelationship(cause, record).Apply();

            return true;
        }

        public static HasBeenLord Execute(
            Cosmos cosmos,
            Person person,
            Location location)
        {
            location.SetLord(person);
            if (person != null)
            {
                if (!person.Attributes.OfType<HasBeenLord>().Any())
                    person.Events.TryAdd(new LordshipEvent(cosmos, person));
                var lordship = new HasBeenLord(person, location, cosmos.Time);
                person.Attributes.Add(lordship);
                return lordship;
            }
            return null;
        }
    }
}
