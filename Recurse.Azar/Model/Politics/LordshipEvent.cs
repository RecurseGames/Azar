﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Maths.Hashing;
using Recurse.Common.Scheduling.Events;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Mortality;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Politics
{
    public class LordshipEvent : MutableEvent<Time<Duration>?>
    {
        private readonly Cosmos _cosmos;

        private readonly Person _person;

        private IEnumerable<Location> RuledLocations
        {
            get { return _person.Attributes.OfType<HasBeenLord>().Where(a => a.IsActive).Select(a => a.Target); }
        }

        public LordshipEvent(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;

            UpdateTime();
        }

        public override void Occur()
        {
            var neighbours = RuledLocations
                .SelectMany(l => l.Neighbours)
                .Distinct()
                .Where(n => !Equals(GetAllegiance.Execute(n.Lord as Person), GetAllegiance.Execute(_person)))
                .ToArray();
            if (neighbours.Any())
            {
                TryExpand(_cosmos.Rng.Select(neighbours));
            }
            UpdateTime();
        }

        private void TryExpand(Location location)
        {
            var activeLordships = RuledLocations.Count();
            if (activeLordships < 1) return;

            if (location.Lord == null &&
                _cosmos.Rng.NextDouble() < Math.Pow(Trait.PeacefulExpansion.Get(_person), 2) * 2 - 1)
            {
                GainTerritory.TryExecute(_cosmos, _person, location);
            }
            else if (
                location.Lord != null &&
                _cosmos.Rng.NextDouble() < Math.Pow(Trait.ViolentExpansion.Get(_person), 2) * 2 - 1)
            {
                var attackRecord = new ActionRecord(_cosmos.CreateInstant(), Singleton<AttackVerb>.Instance)
                {
                    Subjects = { _person, new TempEntity(_person.Sexuality.Pronouns[PronounType.Their].Hyperstring + " armies") },
                    Objects = { location }
                };
                _person.Records.Add(attackRecord);
                location.Records.Add(attackRecord);
                location.Lord.Records.Add(attackRecord);

                var defeated = _cosmos.Rng.NextDouble() < Personality.Merge(
                    Trait.MilitaryMight.Get(_person),
                    1 - Trait.MilitaryMight.Get(location.Lord)) ?
                    location.Lord :
                    _person;
                var defeatRecord = new ActionRecord(_cosmos.CreateInstant(), new IsAdjective("deafeated"))
                {
                    Subjects = { defeated }
                };
                new CausalRelationship(attackRecord, defeatRecord);
                
                if (Equals(location.Lord, defeated))
                {
                    var killRecord = new ActionRecord(_cosmos.CreateInstant(), new IsAdjective("killed"))
                    {
                        Subjects = { defeated },
                        Type = RecordType.Political
                    };
                    new CausalRelationship(attackRecord, killRecord);
                    CauseDeath.Execute(_cosmos, (Person)location.Lord, DeathType.Conflict, killRecord, true);
                    GainTerritory.TryExecute(_cosmos, _person, location, attackRecord);
                }
            }
            else if (
                location.Lord != null &&
                _cosmos.Rng.NextDouble() < Math.Pow(Trait.VassalExpansion.Get(_person), 2) * 2 - 1)
            {
                GainTerritory.TryExecute(_cosmos, _person, location);
            }
        }

        private void UpdateTime()
        {
            Time = _cosmos.Time + GeometricDistribution.GetNext(
                _cosmos.Rng,
                Math.Pow(Trait.PoliticalActivity.Get(_person), 2) * .5) * Durations.Year;
        }
    }
}
