﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Scheduling.Events;
using Recurse.Common.Utils;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Mortality;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Politics;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Occupations
{
    public class AssumeLeadershipEvent : MutableEvent<Time<Duration>?>
    {
        private readonly Person _person;

        private readonly Cosmos _cosmos;

        public AssumeLeadershipEvent(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;
            Time = 
                cosmos.Time + 
                GeometricDistribution.GetNext(cosmos.Rng, .3) * 30 * Durations.Year;
        }

        public override void Occur()
        {
            var location = _person.Location.Get();

            if (Age.GetYears(_person, _cosmos.Time) < 18)
            {
                Time =
                    _cosmos.Time +
                    GeometricDistribution.GetNext(_cosmos.Rng, .3) * 30 * Durations.Year;
                return;
            }

            if (location.Lord == null)
            {
                GainTerritory.TryExecute(_cosmos, _person, location);
            }
            else if (_cosmos.Rng.NextDouble() < Trait.RebelChance.Get(_person) - .8)
            {
                var rebellion = new ActionRecord(_cosmos.CreateInstant(), Singleton<RebelAgainst>.Instance)
                {
                    Subjects = { _person },
                    Objects = { location.Lord },
                    Type = RecordType.Political
                };
                _person.Records.Add(rebellion);
                location.Records.Add(rebellion);
                location.Lord.Records.Add(rebellion);

                var personExecuted =
                    _cosmos.Rng.NextDouble() < .5 ||
                    _cosmos.Rng.NextDouble() < Trait.PoliticalStrength.Get(location.Lord) ?
                    _person : location.Lord;
                var execution = new ActionRecord(_cosmos.CreateInstant(), Singleton<IsExecuted>.Instance)
                {
                    Subjects = { personExecuted },
                    Type = RecordType.Political
                };
                _person.Records.Add(execution);
                location.Records.Add(execution);
                location.Lord.Records.Add(execution);

                new CausalRelationship(rebellion, execution).Apply();

                CauseDeath.Execute(
                    _cosmos,
                    (Person) personExecuted,
                    DeathType.Execution,
                    execution,
                    true);

                if (!Equals(_person, personExecuted))
                    GainTerritory.TryExecute(_cosmos, _person, location, rebellion);
            }

            Time = null;
            IsFixed = true;
        }
    }
}
