﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Politics
{
    public class Monarchy : Entity
    {
        public readonly Location Capital;

        public readonly List<Person> Vassals = new List<Person>();

        private readonly List<Person> _allMonarchs = new List<Person>();

        public Person Monarch { get; private set; }

        public int TotalMonarchs { get { return _allMonarchs.Count; } }

        public Monarchy(Location location, Time<Duration> start)
        {
            Capital = location;
        }

        public void SetMonarch(Person person)
        {
            Monarch = person;
            if (person != null)
            {
                _allMonarchs.Add(person);
            }
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            return _allMonarchs.Cast<Entity>();
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            yield return RecordType.Marriage;
            yield return RecordType.Conversion;
            yield return RecordType.Political;
            yield return RecordType.Birth;
            yield return RecordType.Death;
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield break;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(aliases, "The State of " + Capital.CustomName, null, this);
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }
    }
}
