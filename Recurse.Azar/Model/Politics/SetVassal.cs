﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class SetVassal
    {
        public static bool TryExecute(Cosmos cosmos, Monarchy monarchy, Person vassal)
        {
            if (vassal.Attributes.OfType<HasBeenMonarch>().Any(a => a.IsActive))
                return false;
            if (vassal.Attributes.OfType<HasBeenVassal>().Any(a => a.IsActive))
                return false;

            vassal.Attributes.Add(new HasBeenVassal(vassal, monarchy, cosmos.Time));
            monarchy.Vassals.Add(vassal);

            var pledgeRecord = new ActionRecord(cosmos.CreateInstant(), Singleton<PledgeAllegianceVerb>.Instance)
            {
                Subjects = { vassal },
                Objects = { monarchy.Monarch },
                Type = RecordType.Political
            };
            vassal.Records.Add(pledgeRecord);
            monarchy.Records.Add(pledgeRecord);
            foreach (var lordship in vassal.Attributes.OfType<HasBeenLord>().Where(a => a.IsActive))
            {
                lordship.Target.Records.Add(pledgeRecord);
            }

            return true;
        }
    }
}
