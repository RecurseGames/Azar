﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Politics
{
    public abstract class HasHeldOffice : IAttribute, IDenotesTitle
    {
        private readonly Time<Duration> _start;

        public abstract bool IsActive
        {
            get;
        }

        public abstract NobleTitle Title
        {
            get;
        }

        public abstract double Importance
        {
            get;
        }

        public Time<Duration> Start
        {
            get { return _start; }
        }

        public HasHeldOffice(Time<Duration> start)
        {
            _start = start;
        }
    }

    public abstract class HasHeldOffice<TTarget> : HasHeldOffice
    {
        private Person _subject;

        private readonly TTarget _target;

        public TTarget Target
        {
            get { return _target; }
        }

        public override bool IsActive
        {
            get { return GetCurrentHolders().Contains(_subject); }
        }

        public HasHeldOffice(Person subject, TTarget target, Time<Duration> start) : base(start)
        {
            _subject = subject;
            _target = target;
        }

        protected abstract IEnumerable<Person> GetCurrentHolders();
    }
}
