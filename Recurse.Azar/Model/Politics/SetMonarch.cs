﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class SetMonarch
    {
        public static void Execute(Cosmos cosmos, Monarchy monarchy, Person candidate, IRecord cause = null)
        {
            if (candidate == null || 
                candidate.Attributes.OfType<HasBeenMonarch>().Any(a => a.IsActive) ||
                candidate.Attributes.OfType<HasBeenVassal>().Any(a => a.IsActive))
            {
                monarchy.SetMonarch(null);
                return;
            }

            var isMonarch = new HasBeenMonarch(candidate, monarchy, cosmos.Time);
            monarchy.SetMonarch(candidate);
            candidate.Attributes.Add(isMonarch);
            candidate.Events.TryAdd(new LordshipEvent(cosmos, candidate));

            StartLordship.Execute(cosmos, candidate, monarchy.Capital);

            var ascensionRecord = new ActionRecord(cosmos.CreateInstant(), Singleton<BecomeVerb>.Instance)
            {
                Subjects = { candidate },
                Objects = { isMonarch.Title },
                Type = RecordType.Political
            };
            candidate.Records.Add(ascensionRecord);
            monarchy.Records.Add(ascensionRecord);
            monarchy.Capital.Records.Add(ascensionRecord);

            if (cause != null)
                new CausalRelationship(cause, ascensionRecord).Apply();
        }
    }
}
