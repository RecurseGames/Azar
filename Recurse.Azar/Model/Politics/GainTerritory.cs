﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class GainTerritory
    {
        public static void TryExecute(Cosmos cosmos, Person actor, Location target, IRecord cause = null)
        {
            // If we already own this location, do nothing
            if (Equals(GetAllegiance.Execute(actor), GetAllegiance.Execute((Person)target.Lord)))
                throw new ArgumentException("Territory already owned by us or our monarch.");

            // Simply claim a location, if possible
            if (target.Lord == null && StartLordship.TryExecute(cosmos, actor, target, cause))
            {
                return;
            }

            // If we cannot claim simply claim (either someone's already there, or we're stretched too thin),
            // the territory must be added to a Monarchy
            var monarchy = actor.Attributes
                .OfType<HasBeenMonarch>()
                .Where(a => a.IsActive)
                .Select(a => a.Target)
                .SingleOrDefault();
            if (monarchy == null)
            {
                monarchy = actor.Attributes
                    .OfType<HasBeenVassal>()
                    .Where(a => a.IsActive)
                    .Select(a => a.Target)
                    .SingleOrDefault();
            }
            if (monarchy == null)
            {
                var capital = actor.Attributes.OfType<HasBeenLord>().Where(a => a.IsActive).First().Target;

                monarchy = new Monarchy(capital, cosmos.Time);
                actor.Attributes.Add(new HasBeenMonarch(actor, monarchy, cosmos.Time));
                monarchy.SetMonarch(actor);

                var establishRecord = new ActionRecord(cosmos.CreateInstant(), Singleton<EstablishVerb>.Instance)
                {
                    Subjects = { actor },
                    Objects = { monarchy },
                    Type = RecordType.Political
                };
                actor.Records.Add(establishRecord);
                monarchy.Records.Add(establishRecord);
                capital.Records.Add(establishRecord);
            }

            if (target.Lord != null)
            {
                SetVassal.TryExecute(cosmos, monarchy, (Person)target.Lord);
            }
            else
            {
                var availableVassals = new List<Person>(actor.Location.Get().Contents.OfType<Person>().Where(p => !p.IsDead));
                availableVassals.Remove(actor);
                if (availableVassals.Any())
                {
                    var vassal = cosmos.Rng.Select(availableVassals);
                    if (SetVassal.TryExecute(cosmos, monarchy, vassal))
                    {
                        StartLordship.TryExecute(cosmos, vassal, target, cause);
                    }
                }
            }
        }
    }
}
