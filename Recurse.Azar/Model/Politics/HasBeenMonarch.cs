﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Politics
{
    public class HasBeenMonarch : HasHeldOffice<Monarchy>
    {
        private readonly NobleTitle _title;

        public override NobleTitle Title
        {
            get { return _title; }
        }

        public override double Importance
        {
            get { return 3; }
        }

        public HasBeenMonarch(Person subject, Monarchy target, Time<Duration> start) : base(subject, target, start)
        {
            _title = new NobleTitle(target.Capital) { Rank = subject.Sexuality.MonarchTitle };
        }

        protected override IEnumerable<Person> GetCurrentHolders()
        {
            yield return Target.Monarch;
        }
    }
}
