﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class GetAllegiance
    {
        public static Person Execute(Person person)
        {
            if (person == null)
                return null;

            var isMonarch = person.Attributes.OfType<HasBeenMonarch>().SingleOrDefault(a => a.IsActive);
            if (isMonarch != null)
                return person;

            var isVassal = person.Attributes.OfType<HasBeenVassal>().SingleOrDefault(a => a.IsActive);
            if (isVassal != null)
                return isVassal.Target.Monarch;

            return person;
        }
    }
}
