﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Records;
using System.Linq;

namespace Recurse.Azar.Model.Politics
{
    public static class GiveTitles
    {
        public static void Execute(Cosmos cosmos, Person donor, Person recipient, IRecord cause)
        {
            var donorMonarchy = donor.Attributes.OfType<HasBeenMonarch>().Where(a => a.IsActive).SingleOrDefault();
            if (donorMonarchy != null)
            {
                SetMonarch.Execute(cosmos, donorMonarchy.Target, recipient, cause);
            }

            var donorLordships = donor.Attributes.OfType<HasBeenLord>().Where(a => a.IsActive).ToList();
            if (donorLordships != null)
            {
                foreach (var lordship in donorLordships)
                {
                    StartLordship.TryExecute(cosmos, recipient, lordship.Target, cause);
                }
            }
        }
    }
}
