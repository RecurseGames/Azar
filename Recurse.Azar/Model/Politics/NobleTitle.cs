﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Politics
{
    public class NobleTitle : Entity
    {
        private readonly Entity _subject;

        public Hyperstring Text
        {
            get
            {
                return 
                    Hyperstring.Empty +
                    Rank + 
                    "of " + 
                    _subject.GetSimpleReference().GetNoun(PronounType.They).Hyperstring;
            }
        }

        public string Rank
        {
            get; set;
        }

        public NobleTitle(Entity subject)
        {
            _subject = subject;
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield break;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new TempEntity(Text);
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            throw new InvalidOperationException();
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            yield break;
        }
    }
}
