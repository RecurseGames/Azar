﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Records;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Locations;
using System.Linq;
using Recurse.Azar.Model.Attributes;

namespace Recurse.Azar.Model.Residency
{
    public class ResidenceRecord : ActionRecord
    {
        public readonly Location Residence;

        public ResidenceRecord(Instant instant, Location residence) : base(instant, null)
        {
            Residence = residence;
        }

        public ResidenceRecord(Instant instant, GrammarAction action, Location residence) : base(instant, action)
        {
            Residence = residence;
        }

        public static bool HaveSameResidence(Entity a, Entity b)
        {
            var recordA = a.Records.OfType<ResidenceRecord>().LastOrDefault();
            var recordB = b.Records.OfType<ResidenceRecord>().LastOrDefault();
            return
                recordA != null &&
                recordB != null &&
                Equals(recordA.Residence, recordB.Residence);
        }
    }
}
