﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Maths.Interpolations;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Residency;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Tracking;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.People.Events
{
    public class ConsiderEmigration : PersonEvent
    {
        public static readonly TrackedCount TotalExecuted = new TrackedCount("Emigrations");

        public static readonly TrackedList<int> TotalExecutedSizes = new TrackedList<int>("Emigration Sizes");

        private const double MaxAge = 80;

        private readonly LinearInterpolation AgeToAnnualRate = new LinearInterpolation
        {
            { 16, 0 },
            { 18, .3 },
            { 35, .1 },
            { 55, .05 },
            { MaxAge, 0 }
        };

        public double Chance
        {
            get
            {
                var lastEmigration = Person.Records.LastOrDefault<ResidenceRecord>();
                if (lastEmigration != null)
                {
                    var minNext = 
                        lastEmigration.Time +
                        Durations.Year * (1 + 10 * Trait.EmigrationChance.Get(Person));
                    if (Time.Value.CompareTo(minNext) < 0)
                        return 0;
                }
                
                var result = Math.Pow(Trait.EmigrationChance.Get(Person), 2) * .2;

                // People more likely to emigrate when single. This avoids locations filled
                // with single people dying out
                if (!Person.Attributes.OfType<HasRomance>().Any())
                    result *= 1.5;

                return result;
            }
        }

        public ConsiderEmigration(Cosmos cosmos, Person person) : base(cosmos, person)
        {
            UpdateTime();
        }

        public override void Occur()
        {
            UpdateTime();
            if (Cosmos.Rng.NextDouble() < Chance)
            {
                var location = Person.Location.Get();
                if (location != null)
                {
                    var newLocation = Cosmos.Rng.Select(location.Neighbours.ToArray());
                    var people = new List<Person> { Person };
                    var record = new ResidenceRecord(
                        Cosmos.CreateInstant(),
                        Singleton<EmigrateTo>.Instance,
                        newLocation)
                    {
                        Objects = { newLocation }
                    };
                    GatherCompanions(Cosmos, record, Person);
                    foreach (Person person in record.Subjects)
                    {
                        person.Records.Add(record);
                        person.Location.Set(record.Time, newLocation);
                    }
                    TotalExecuted.TryAdd(Cosmos.Tracker);
                    TotalExecutedSizes.TryAdd(Cosmos.Tracker, record.Subjects.Count);
                }
            }
        }

        private void UpdateTime()
        {
            var age = Age.GetYears(Person, Cosmos.Time);
            Time = Cosmos.Time + Durations.Year * SampledGeometricDistribution.GetNext(
                Cosmos.Rng,
                age,
                MaxAge,
                5,
                AgeToAnnualRate.Get);
        }

        private static void GatherCompanions(
            Cosmos cosmos,
            ActionRecord emigrationRecord,
            Person person)
        {
            if (emigrationRecord.Subjects.Contains(person))
                throw new InvalidOperationException("Already travelling");
            emigrationRecord.Subjects.Add(person);

            // Bring partners
            foreach (var romance in person.Attributes.OfType<HasRomance>().Select(r => r.Romance).ToArray())
            {
                var partner = romance.GetPartnerOrNull(person);
                if (!ResidenceRecord.HaveSameResidence(person, partner) ||
                    emigrationRecord.Subjects.Contains(partner))
                    continue;

                if (romance.IsOfficial ||
                    cosmos.Rng.NextDouble() < .5 * Trait.ClosenessToUnofficialPartners.Get(partner))
                {
                    GatherCompanions(cosmos, emigrationRecord, partner);
                }
                else if (cosmos.Rng.NextDouble() < Trait.LikesLongDistanceRomance.Get(partner))
                {
                    EndRomance.Execute(cosmos, romance, emigrationRecord);
                }
            }

            // Bring children
            foreach (var child in ParentageRelationship.GetChildren(person))
            {
                if (!ResidenceRecord.HaveSameResidence(person, child) ||
                    emigrationRecord.Subjects.Contains(child))
                    continue;

                var childAge = Age.GetYears(child, cosmos.Time);
                if (childAge < 18 || cosmos.Rng.NextDouble() < .5)
                    GatherCompanions(cosmos, emigrationRecord, child);
            }
        }
    }
}
