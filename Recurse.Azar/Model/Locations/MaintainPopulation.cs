﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Model;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Events;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Sex.Reproduction;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar
{
    public class MaintainPopulation : MutableEvent<Time<Duration>?>
    {
        private const int Minimum = 20;

        private readonly Cosmos _cosmos;

        private Location _location;

        public MaintainPopulation(Cosmos cosmos, Location location)
        {
            _cosmos = cosmos;
            _location = location;

            Time = cosmos.Time;
        }

        public override void Occur()
        {
            var population = _location.Contents.OfType<Person>().Count(p => !p.IsDead);
            Time += Durations.Year * GeometricDistribution.GetNext(
                _cosmos.Rng,
                Math.Pow(Minimum / (Minimum + population), 2));
            if (population < Minimum)
            {
                var person = CreatePerson.Execute(_cosmos);
                CauseBirth.Execute(_cosmos, person, _location);
                AddToBloodline.Execute(_cosmos, person);
                ActivatePerson.Execute(_cosmos, person);
            }
            var maxTime = _cosmos.Time + Durations.Year;
            if (Time.Value.CompareTo(maxTime) > 0)
                Time = maxTime;
        }
    }
}
