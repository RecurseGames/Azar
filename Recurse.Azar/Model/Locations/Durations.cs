﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model
{
    public static class Durations
    {
        public static readonly Measurement<Duration> Day = new Measurement<Duration>(new Duration(1));

        public static readonly Measurement<Duration> Week = Day * 7;

        public static readonly Measurement<Duration> Month = Week * 4;

        public static readonly Measurement<Duration> Year = Month * 12;

        public static int GetYear(Time<Duration> time)
        {
            var timePassed = time.GetDifference(default(Time<Duration>));
            return (int)Math.Floor(timePassed / Year);
        }
    }
}
