﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using System;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model
{
    public class Season
    {
        // Sowing Season, Fire Season, Storm Season, Harvest Season, Cold Season?
        public static readonly Season
            Spring = new Season("Spring"),
            Summer = new Season("Summer"),
            Autumn = new Season("Autumn"),
            Winter = new Season("Winter");

        public static readonly List<Season> List = new List<Season>
        {
            Spring, Summer, Autumn, Winter
        };

        public static readonly Measurement<Duration> Duration = Durations.Year / List.Count;

        public static int GetNumber(Time<Duration> time)
        {
            var timePassed = time.GetDifference(default(Time<Duration>));
            return (int)Math.Floor(timePassed / Duration);
        }

        public static Season Get(Time<Duration> time)
        {
            return List[GetNumber(time) % List.Count];
        }

        public readonly string Name;

        public Season(string name)
        {
            Name = name;
        }
    }
}
