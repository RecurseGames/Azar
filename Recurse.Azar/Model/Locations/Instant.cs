﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Hashing;
using Recurse.Ianna.Model;
using Recurse.RiseAndFall.Model.Locations;
using System;

namespace Recurse.Azar.Model.Locations
{
    public struct Instant : IComparable<Instant>, IEquatable<Instant>
    {
        public readonly Time<Duration> Time;

        public readonly uint Id;

        public Instant(Time<Duration> time, uint id)
        {
            Time = time;
            Id = id;
        }

        public Measurement<Duration> GetDifference(Instant other)
        {
            return Time.GetDifference(other.Time);
        }

        public int CompareTo(Instant other)
        {
            return Equals(Time, other.Time) ?
                Id.CompareTo(other.Id) :
                Time.CompareTo(other.Time);
        }

        public bool Equals(Instant other)
        {
            return Equals(Time, other.Time) && Equals(Id, other.Id);
        }

        public override bool Equals(object obj)
        {
            return obj is Instant ? Equals((Instant)obj) : base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return MurmurHash.Default.Hash(Time, Id);
        }
    }
}
