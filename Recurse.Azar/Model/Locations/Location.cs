﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Collections.Planes;
using Recurse.Common.Extensions;
using Recurse.Common.Maths.Space.Metrics;
using Recurse.Common.Maths.Space.Points;
using Recurse.Common.Utils;
using System.Collections.Generic;
using System.Linq;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Politics;
using Recurse.Azar.Model.People;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Attributes
{
    public class Location : Entity
    {
        private readonly IPlane<Location> _grid;

        private readonly XY<int> _coordinate;

        public IEnumerable<Location> Neighbours
        {
            get { return GetRawNeighbours().Where(n => n != null); }
        }

        public XY<double> SpatialCoordinate
        {
            get
            {
                return new XY<double>(
                    _coordinate.X * 0.866,
                    _coordinate.Y + _coordinate.X % 2 == 0 ? 0 : 0.5);
            }
        }

        public IEnumerable<Entity> Contents
        {
            get { return PastContents.Where(e => Equals(e.Location.Get(), this)); }
        }

        public readonly List<Entity> PastContents = new List<Entity>();

        public string CustomName { get; set; }

        public Entity Lord { get; private set; }

        private readonly List<Entity> _pastLords = new List<Entity>();

        public Location(Cosmos cosmos, IPlane<Location> grid, XY<int> coordinate)
        {
            _grid = grid;
            _coordinate = coordinate;

            var name = cosmos.Rng.NextDouble() < .25 ?
                NameBuilder.Syllables.CreateString(cosmos.Rng, 5)
                    .Concat(new[] { new Syllable(" ") })
                    .Concat(NameBuilder.Syllables.CreateString(cosmos.Rng, 3))
                    .ToArray() :
                NameBuilder.Syllables.CreateString(cosmos.Rng, cosmos.Rng.Next(3) + 4);

            CustomName = StringUtils.TitleCase(string.Join("", name.Select(s => s.Text).ToArray()));
        }

        public void SetLord(Entity lord)
        {
            Lord = lord;
            if (lord != null)
                _pastLords.Add(lord);
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            return PastContents
                .Where(GetIsImportant.Execute)
                .Concat(_pastLords);
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            yield return RecordType.Conversion;
            yield return RecordType.Political;
            yield return RecordType.Birth;
            yield return RecordType.Death;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(aliases, CustomName ?? "an unnamed location", null, this);
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield return new CategoryRecord(
                this,
                "province near",
                Neighbours.Select(n => n.GetReference(context).GetNoun(PronounType.Them).Hyperstring).ToArray());
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public IEnumerable<Location> GetCloserNeighbours(Location target)
        {
            if (!Equals(_grid, target._grid))
                yield break;

            var distance = Distance.Euclidean.Measure(_coordinate, target._coordinate);
            foreach (var neighbour in Neighbours)
            {
                var neighbourDistance = Distance.Euclidean.Measure(
                    neighbour._coordinate, 
                    target._coordinate);
                if (neighbourDistance < distance)
                    yield return neighbour;
            }
        }

        private IEnumerable<Location> GetRawNeighbours()
        {
            yield return _grid[_coordinate + new XY<int>(0, 1)];
            yield return _grid[_coordinate + new XY<int>(0, -1)];
            yield return _grid[_coordinate + new XY<int>(1, 0)];
            yield return _grid[_coordinate + new XY<int>(-1, 0)];
            yield return _grid[_coordinate + new XY<int>(1, _coordinate.X % 2 == 0 ? -1 : 1)];
            yield return _grid[_coordinate + new XY<int>(-1, _coordinate.X % 2 == 0 ? -1 : 1)];
        }
    }
}
