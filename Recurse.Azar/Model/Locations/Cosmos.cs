﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Scheduling;
using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People;
using System;
using System.Collections.Generic;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model
{
    public class Cosmos : ITimeSource<Time<Duration>>
    {
        #region Backing Fields

        private readonly Random _rng;

        private readonly CompositeEvent<Time<Duration>> _events = new CompositeEvent<Time<Duration>>();

        private readonly Tracker _tracker = new Tracker();

        private Time<Duration> _time;

        #endregion

        private uint _nextInstantId;

        public Time<Duration> Time
        {
            get { return _time; }
            set
            {
                if (double.IsNaN(value.OriginDistance / Durations.Year))
                    throw new ArgumentException("value", "Time cannot be NaN");
                _time = value;
            }
        }
        
        public Random Rng { get { return _rng; } }

        public CompositeEvent<Time<Duration>> Events { get { return _events; } }
        
        public Tracker Tracker { get { return _tracker; } }

        public Cosmos(Random rng)
        {
            _rng = rng;
        }

        public Instant CreateInstant()
        {
            return new Instant(Time, _nextInstantId++);
        }
    }
}
