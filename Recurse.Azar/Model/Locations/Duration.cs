﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Locations;
using Recurse.RiseAndFall.Model.Locations;
using System;

namespace Recurse.Ianna.Model
{
    public struct Duration : IMeasurementUnit
    {
        private readonly double _value;

        public Duration(double value)
        {
            _value = value;
        }

        public double ToDouble()
        {
            return _value;
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}
