﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Collections.Planes;
using Recurse.Common.Maths.Space.Areas;
using Recurse.Azar.Model.Attributes;

namespace Recurse.Azar.Model
{
    public class Grid : ArrayPlane<Location>
    {
        public Grid(Cosmos cosmos, Rect<int> area) : base(area)
        {
            foreach (var point in Area.GetPoints())
            {
                var location = new Location(cosmos, this, point);
                location.Events.TryAdd(new MaintainPopulation(cosmos, location));
                cosmos.Events.TryAdd(location.Events);
                this[point] = location;
            }
        }
    }
}
