﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Common.Text;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using System.Linq;

namespace Recurse.Azar.Model.Chapters
{
    public class Biography : Chapter
    {
        private readonly Entity _subject;

        private readonly bool _includeRelatives;

        public Biography(Entity subject, bool includeRelatives = false)
        {
            _includeRelatives = includeRelatives;
            _subject = subject;
        }

        protected override IEnumerable<Hypersymbol> ToFragments()
        {
            var renderer = new RecordRenderer();
            renderer.Context.Focus = _subject;
            renderer.Context.Introductions.Add(renderer.Context.Focus);
            foreach (var record in _subject.GetIntroduction(renderer.Context))
            {
                var rendering = renderer.TryRender(record);
                yield return rendering;
            }

            var records = new List<KeyValuePair<Entity, IRecord>>();
            records.AddRange(_subject.Records.OfType<IRecord>()
                .Select(r => new KeyValuePair<Entity, IRecord>(_subject, r)));
            if (_includeRelatives)
            {
                foreach (var relative in _subject.GetRelatives())
                {
                    records.AddRange(relative.Records.OfType<IRecord>()
                        .Select(r => new KeyValuePair<Entity, IRecord>(relative, r)));
                }
            }
            records = records.OrderBy(e => e.Value.Time).ToList();

            var outputRecords = new HashSet<IRecord>();
            foreach (var entry in records)
            {
                if (Equals(entry.Key, _subject) ||
                    !entry.Value.Time.HasValue ||
                    _subject.GetRelavantRecords(entry.Key, entry.Value.Time.Value).Contains(entry.Value.Type))
                {
                    if (outputRecords.Add(entry.Value))
                    {
                        var rendering = renderer.TryRender(entry.Value);
                        if (rendering != null)
                        {
                            yield return rendering;
                        }
                    }
                }
            }
        }
    }
}
