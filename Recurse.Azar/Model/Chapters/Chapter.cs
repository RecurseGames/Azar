﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using System.Collections.Generic;

namespace Recurse.Azar.Model.Chapters
{
    public abstract class Chapter
    {
        public Hyperstring ToSequence()
        {
            return new Hyperstring.Simple(ToFragments());
        }

        protected abstract IEnumerable<Hypersymbol> ToFragments();
    }
}
