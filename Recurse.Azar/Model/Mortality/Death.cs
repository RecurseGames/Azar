﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Text;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Model.People.Records
{
    public class Death
    {
        private readonly Entity _victim;

        public Death(Entity deceased)
        {
            _victim = deceased;
        }
    }
}
