﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using System;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Common.Extensions;
using RG.Saga.Model.Timing.Randomised;
using System.Collections.Generic;

namespace Recurse.Azar.Model.People.Records
{
    public class PlagueStart : Stage
    {
        private readonly Person _victim;

        private readonly Cosmos _cosmos;

        private readonly Plague _plague;

        private readonly bool _isTerminal;

        public PlagueStart(Cosmos cosmos, Person victim, Plague plague)
        {
            _cosmos = cosmos;
            _plague = plague;
            _victim = victim;
            _isTerminal = cosmos.Rng.NextDouble() < .95;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            var victimLocation = _victim.Location.Get();
            if (victimLocation == null) yield break;

            var spreadLocation = _cosmos.Rng.NextDouble() < .03 ?
                _cosmos.Rng.Select(victimLocation.Neighbours.ToArray()) :
                victimLocation;
            var currentCandidates = spreadLocation.Contents.OfType<Person>().Where(p => !p.IsDead);
            var spreadFrequency = Math.Min(currentCandidates.Count() / 200d, .99);

            yield return new Occurence(
                () => 
                {
                    var updatedCandidates = currentCandidates.ToArray();
                    if (updatedCandidates.Any())
                    {
                        var newVictim = _cosmos.Rng.Select(updatedCandidates);
                        _plague.TryAddVictim(newVictim);
                    }
                },
                _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, spreadFrequency) * Durations.Day);

            if (_isTerminal)
            {
                yield return CreateTransition(
                    new PlagueEnd(_cosmos, _victim, _plague, true),
                    _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, .2) * Durations.Day);
            }
            else
            {
                yield return CreateTransition(
                    new PlagueEnd(_cosmos, _victim, _plague, false),
                    _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, .5) * Durations.Week);
            }
        }

        protected override void OnStart()
        {
        }
    }
}
