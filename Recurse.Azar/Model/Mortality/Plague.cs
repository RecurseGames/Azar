﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using Recurse.Azar.Model.Records;
using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using System.Collections.Generic;
using System;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Common.Extensions;
using Recurse.Azar.Model.People.Events;
using Recurse.Azar.Tracking;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Model.Locations;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Politics;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.People.Records
{
    public class Plague : Entity
    {
        public static readonly TrackedList<Plague> Notable = new TrackedList<Plague>("Notable Plagues");

        private readonly string[] Adjectives = new[]
        {
            "Black", "Red", "Yellow", "White", "Burning", "Merciful", "Bloody",
            "Wasting", "Silent", "Laughing", "Kindly", "Gentle", "Creeping",
            "Withering", "Dark", "Cruel", "Damp", "Chilling", "Cold", "Sickening",
            "Bitter", "Trembling", "Sleeping", "Coughing", "Vile"
        };

        #region Backing Fields

        private readonly Entity _causeOfDeath;

        #endregion

        private string _name;

        private readonly int _startYear;

        private readonly HashSet<Person> _immunePeople = new HashSet<Person>();

        private readonly HashSet<Person> _victims = new HashSet<Person>();

        public readonly HashSet<Location> Locations = new HashSet<Location>();

        private readonly Cosmos _cosmos;

        private List<Action> _whenNotable = new List<Action>();

        public bool IsNotable
        {
            get { return _whenNotable == null; }
        }
        
        public Entity CauseOfDeath
        {
            get { return _causeOfDeath; }
        }

        public IEnumerable<Person> Victims
        {
            get { return _victims; }
        }

        public Plague(Cosmos cosmos)
        {
            _causeOfDeath = new AsCauseOfDeath(this);
            _name = string.Format("The {0} Plague", cosmos.Rng.Select(Adjectives));
            _cosmos = cosmos;
            _startYear = Durations.GetYear(cosmos.Time);
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            return Victims.Cast<Entity>().Where(GetIsImportant.Execute);
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            yield return RecordType.Death;
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(
                aliases, 
                _name != null ? (_name + " of " + _startYear.ToString()) : "an unnamed plague",
                Pronoun.It,
                this);
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield return new CategoryRecord(
                this,
                "disease",
                string.Format("which killed {0} people", DurationText.AsRepresentativePopulation(_victims.Count)));
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }

        public void WhenNotable(Action action)
        {
            if (_whenNotable == null) action();
            else _whenNotable.Add(action);
        }

        public bool TryAddVictim(Person victim)
        {
            // Make sure about a third of the population is immune. This will avoid
            // rampant plagues killing everybody
            if (!_immunePeople.Add(victim) || _cosmos.Rng.NextDouble() < .3)
                return false;

            if (!_victims.Add(victim) || !(victim.HealthLifecycle.Stage is Health))
                return false;
            
            if (_victims.Count == 6)
            {
                foreach (var action in _whenNotable)
                    action();
                _whenNotable = null;
                Notable.TryAdd(_cosmos.Tracker, this);
            }

            victim.HealthLifecycle.Stage = new PlagueStart(_cosmos, victim, this);

            var location = victim.Location.Get();
            if (Locations.Add(location))
            {
                WhenNotable(() =>
                {
                    var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<ArriveIn>.Instance)
                    {
                        Subjects = { this },
                        Objects = { location }
                    };
                    victim.Records.Add(record);
                });
            }

            WhenNotable(() =>
            {
                var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<ContractVerb>.Instance)
                {
                    Subjects = { victim },
                    Objects = { this }
                };
                location.Records.Add(record);
            });

            return true;
        }

        private class AsCauseOfDeath : Entity
        {
            private readonly Plague _plague;

            public AsCauseOfDeath(Plague plague)
            {
                _plague = plague;
            }

            public override IEnumerable<IRecord> GetIntroduction(TextContext context)
            {
                yield break;
            }

            public override IGrammarObject GetSimpleReference(AliasMapping aliases)
            {
                if (!_plague.IsNotable)
                    return new TempEntity("a sudden illness");
                return new NamedGrammarObject(aliases, _plague._name, Pronoun.It, _plague);
            }

            public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
            {
                throw new InvalidOperationException();
            }

            public override IEnumerable<Entity> GetRelatives()
            {
                yield break;
            }

            public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
            {
                yield break;
            }
        }
    }
}
