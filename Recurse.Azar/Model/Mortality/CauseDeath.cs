﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Politics;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Linq;

namespace Recurse.Azar.Model.Mortality
{
    public static class CauseDeath
    {
        public static void Execute(
            Cosmos cosmos,
            Person victim,
            DeathType type,
            ActionRecord record,
            bool preventInheritance = false)
        {
            if (victim.IsDead)
                throw new ArgumentException("victim", "Victim is already dead.");
            
            var isDead = new IsDead(cosmos.Time, type);
            victim.Attributes.Add(isDead);

            record.Errata.Add(Age.Describe(victim, record.Time));
            
            victim.Records.Add(record);
            victim.Events.Cancel();
            foreach (var romance in victim.Attributes.OfType<HasRomance>().Select(r => r.Romance).ToArray())
            {
                EndRomance.Execute(cosmos, romance, record);
            }

            foreach (var heldOffice in victim.Attributes.OfType<HasHeldOffice>().Where(a => a.IsActive))
            {
                record.Errata.Add(new CategoryRecord(victim, null, heldOffice.Title.Text)
                {
                    Duration = cosmos.Time.GetDifference(heldOffice.Start)
                });
            }

            var heir = preventInheritance ? null : ParentageRelationship
                .GetChildren(victim)
                .Where(c => !c.IsDead)
                .FirstOrDefault();
            GiveTitles.Execute(cosmos, victim, heir, record);
        }
    }
}
