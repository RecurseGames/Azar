﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People.Records;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Mortality
{
    public class IsDead : IAttribute
    {
        public readonly Time<Duration> TimeOfDeath;

        public readonly DeathType TypeOfDeath;

        public IsDead(Time<Duration> timeOfDeath, DeathType typeOfDeath)
        {
            TypeOfDeath = typeOfDeath;
            TimeOfDeath = timeOfDeath;
        }
    }
}
