﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Interpolations;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.People.Records;
using RG.Saga.Model.Timing.Randomised;
using System.Collections.Generic;

namespace Recurse.Azar.Model.People.Events
{
    public class Health : Stage
    {
        private readonly LinearInterpolation AgeToAnnualIllnessRate = new LinearInterpolation
        {
            { 0, .003 },
            { 65, .005 },
            { 90, .1 },
            { 100, .5 }
        };

        private readonly Cosmos _cosmos;

        private readonly Person _person;

        public Health(Cosmos cosmos, Person person)
        {
            _cosmos = cosmos;
            _person = person;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return new Occurence(
                () => new Plague(_cosmos).TryAddVictim(_person),
                _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, .02) * 70 * Durations.Year);

            var nextIllnessTime = _cosmos.Time + SampledGeometricDistribution.GetNext(
                _cosmos.Rng,
                Age.GetYears(_person, _cosmos.Time),
                5,
                AgeToAnnualIllnessRate.Get) * Durations.Year;
            yield return CreateTransition(
                new IllnessStart(_cosmos, _person),
                nextIllnessTime);
        }

        protected override void OnStart()
        {
        }
    }
}
