﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Maths.Interpolations;
using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Mortality;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Collections.Generic;

namespace Recurse.Azar.Model.People.Events
{
    public class IllnessEnd : Stage
    {
        private readonly LinearInterpolation AgeToDeathRate = new LinearInterpolation
        {
            { 0, .4 },
            { 35, .3 },
            { 55, .6 },
            { 100, .99 }
        };

        private readonly Cosmos _cosmos;

        private readonly Person _victim;

        private readonly Illness _illness;

        public IllnessEnd(Cosmos cosmos, Person victim, Illness illness)
        {
            _illness = illness;
            _cosmos = cosmos;
            _victim = victim;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return CreateTransition(
                new Health(_cosmos, _victim),
                _cosmos.Time + Durations.Month);
        }

        protected override void OnStart()
        {
            var deathChance = AgeToDeathRate.Get(Age.GetYears(_victim, _cosmos.Time));
            if (_cosmos.Rng.NextDouble() < deathChance)
            {
                var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<DieFromVerb>.Instance)
                {
                    Subjects = { _victim },
                    Objects = { new TempEntity("an illness") },
                    Type = RecordType.Death
                };
                CauseDeath.Execute(
                    _cosmos,
                    _victim,
                    DeathType.NaturalCauses,
                    record);
            }
            else
            {
                _illness.Start.Errata.Add(new ActionRecord(_cosmos.CreateInstant(), Singleton<RecoverFromVerb>.Instance)
                {
                    Subjects = { _victim }
                });
            }
        }
    }
}
