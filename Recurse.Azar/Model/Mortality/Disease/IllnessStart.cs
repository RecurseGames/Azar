﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using System;
using System.Collections.Generic;

namespace Recurse.Azar.Model.People.Events
{
    public class IllnessStart : Stage
    {
        private readonly Cosmos _cosmos;

        private readonly Person _victim;

        private readonly Illness _illness;

        public IllnessStart(Cosmos cosmos, Person victim)
        {
            _cosmos = cosmos;
            _victim = victim;
            _illness = new Illness();
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return CreateTransition(
                new IllnessEnd(_cosmos, _victim, _illness),
                _cosmos.Time + _cosmos.Rng.Next(3, 30) * Durations.Day);
        }

        protected override void OnStart()
        {
            _illness.Start = new ActionRecord(_cosmos.CreateInstant(), Singleton<FallIllVerb>.Instance)
            {
                Subjects = { _victim }
            };
            _victim.Records.Add(_illness.Start);
        }
    }
}
