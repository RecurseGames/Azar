﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections.Generic;
using Recurse.Common.Text;
using Recurse.Azar.Model.Text;
using System.Linq;
using Recurse.Azar.Model.Records;
using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;

namespace Recurse.Azar.Model.People.Records
{
    public class Illness
    {
        public ActionRecord Start { get; set; }

        //public override GrammarNoun GetRecordEventNoun(AliasMapping pronouns, PronounType type)
        //{
        //    var result =
        //        Hyperstring.Empty +
        //        _owner.GetNoun(pronouns, PronounType.Their).Hyperstring +
        //        "illness";
        //    return result;
        //}
    }
}
