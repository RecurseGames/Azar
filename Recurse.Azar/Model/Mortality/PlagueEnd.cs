﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Azar.Model.Lifecycles;
using RG.Saga.Model.Timing.Randomised;
using Recurse.Azar.Model.People.Events;
using Recurse.Azar.Model.Records;
using Recurse.Common.Utils;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Sex.Romances;
using System.Collections.Generic;
using Recurse.Azar.Model.Mortality;

namespace Recurse.Azar.Model.People.Records
{
    public class PlagueEnd : Stage
    {
        private readonly Person _victim;

        private readonly Cosmos _cosmos;

        private readonly Plague _plague;

        private readonly bool _victimDies;

        public PlagueEnd(Cosmos cosmos, Person victim, Plague plague, bool victimDies)
        {
            _victimDies = victimDies;
            _plague = plague;
            _cosmos = cosmos;
            _victim = victim;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return CreateTransition(
                new Health(_cosmos, _victim),
                _cosmos.Time + GeometricDistribution.GetNext(_cosmos.Rng, 1) * Durations.Month);
        }

        protected override void OnStart()
        {
            if (_victimDies)
            {
                var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<DieFromVerb>.Instance)
                {
                    Subjects = { _victim },
                    Objects = { _plague.CauseOfDeath },
                    Type = RecordType.Death
                };
                CauseDeath.Execute(
                    _cosmos,
                    _victim,
                    DeathType.Plague,
                    record);
            }
            else
            {
                var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<RecoverFromVerb>.Instance)
                {
                    Subjects = { _victim },
                    Objects = { _plague.CauseOfDeath }
                };
                _victim.Records.Add(record);
            }
        }
    }
}
