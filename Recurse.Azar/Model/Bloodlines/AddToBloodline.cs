﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Residency;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Tracking;
using System.Linq;

namespace Recurse.Azar.Model.Sex.Reproduction
{
    public static class AddToBloodline
    {
        public static void Execute(
            Cosmos cosmos,
            Person target,
            params Person[] parents)
        {
            var inheritance = parents
                .Where(p => p != null)
                .Select(p => p.Attributes.OfType<IsInBloodline>().SingleOrDefault())
                .Where(a => a != null)
                .OrderBy(e => e.Generation)
                .LastOrDefault();

            var isBloodlineMember = inheritance != null ?
                new IsInBloodline(inheritance.Bloodline, inheritance.Generation + 1) :
                new IsInBloodline(Bloodline.Create(cosmos), 1);
            target.Attributes.Add(isBloodlineMember);

            target.Bloodline = isBloodlineMember.Bloodline;
            isBloodlineMember.Bloodline.Members.Add(target);
        }
    }
}
