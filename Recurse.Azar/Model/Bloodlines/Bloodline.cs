﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;
using Recurse.Common.Maths.Hashing;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Language.Objects;
using Recurse.Common.Utils;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Personalities;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Locations;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Philosophies
{
    public class Bloodline : Entity
    {
        public readonly string Surname;

        public readonly List<Person> Members = new List<Person>();

        public Trait DefiningTrait
        {
            get; set;
        }

        public double DefiningTraitValue
        {
            get; set;
        }

        public string CollectiveName
        {
            get { return Surname.EndsWith("s") ? Surname : (Surname + "s"); }
        }

        public int HighestGeneration
        {
            get { return Members.Select(p => p.Attributes.OfType<IsInBloodline>().Single().Generation).Max(); }
        }

        public Bloodline(string name)
        {
            Surname = StringUtils.TitleCase(name);
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            return Members.Cast<Entity>();
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            yield return RecordType.Marriage;
            yield return RecordType.Conversion;
            yield return RecordType.Political;
            yield return RecordType.Birth;
            yield return RecordType.Death;
        }

        public static Bloodline Create(Cosmos cosmos)
        {
            var syllables = NameBuilder.Syllables.CreateString(cosmos.Rng, cosmos.Rng.Next(4) + 3);
            var name = StringUtils.TitleCase(string.Join("", syllables.Select(s => s.Text).ToArray()));
            return new Bloodline(name);
        }

        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new NamedGrammarObject(aliases, string.Format("The {0}", CollectiveName), Pronoun.It, this);
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield return new CategoryRecord(this, "family");
            if (DefiningTrait != null)
                yield return new SimpleRecord("they are known for being " + DefiningTrait.GetAdjective(DefiningTraitValue));
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }
    }
}
