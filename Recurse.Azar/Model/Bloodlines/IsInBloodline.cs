﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Locations;
using System.Linq;
using Recurse.Common.Text;
using Recurse.Azar.Model.Attributes;
using Recurse.Ianna.Model;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Philosophies;
using Recurse.Azar.Model.Personalities;

namespace Recurse.Azar.Model.People.Records
{
    public class IsInBloodline : IAttribute, IAffectsTrait
    {
        public readonly Bloodline Bloodline;

        public readonly int Generation;

        public double DefiningTraitValue { get { return Bloodline.DefiningTraitValue; } }

        public Trait DefiningTrait { get { return Bloodline.DefiningTrait; } }

        public IsInBloodline(Bloodline bloodline, int generation)
        {
            Bloodline = bloodline;
            Generation = generation;
        }
    }
}
