﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using Recurse.Azar.Model.People;
using Recurse.Azar.Language.Objects;
using Recurse.Azar.Tracking;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model.Hatred
{
    public class Feud : Entity
    {
        public static readonly TrackedList<Feud> List = new TrackedList<Feud>("Feuds");

        private readonly Person _origin;

        public Feud(Person origin)
        {
            _origin = origin;
        }

        public override IEnumerable<IRecord> GetIntroduction(TextContext context)
        {
            yield break;
        }

        public override IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time)
        {
            throw new InvalidOperationException();
        }

        public override IEnumerable<Entity> GetRelatives()
        {
            yield break;
        }
        
        public override IGrammarObject GetSimpleReference(AliasMapping aliases)
        {
            return new OwnedGrammarObject(
                _origin.GetSimpleReference(aliases),
                new ReferenceSymbol(this, "Feud"));
        }

        public override IEnumerable<IRecord> TryGetShortIntroduction(TextContext context)
        {
            yield break;
        }
    }
}
