﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.Mortality;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Tracking;
using RG.Saga.Model.Timing.Randomised;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Recurse.Azar.Model.Hatred
{
    public class StartEmnity : Stage
    {
        public static readonly TrackedCount Started = new TrackedCount("Emnities");

        private readonly Cosmos _cosmos;

        private readonly ActionRecord _cause;

        private Occurence? _transition = null;

        private readonly Feud _feud;

        public readonly Emnity Emnity;

        public StartEmnity(
            Cosmos cosmos, 
            Emnity emnity,
            Feud feud,
            ActionRecord cause = null)
        {
            _feud = feud;
            _cause = cause;
            _cosmos = cosmos;
            Emnity = emnity;
        }

        public override IEnumerable<Occurence> GetOccurences()
        {
            yield return new Occurence(
                () =>
                {
                    // We kill our enemies! At last, we are at peace.

                    foreach (var target in Emnity.Targets.Where(t => !t.IsDead))
                    {
                        var murder = CauseMurder.Execute(_cosmos, Emnity.Source, target, _feud, _cause);
                        _feud.Records.Add(murder);
                    }
                    if (!_transition.HasValue)
                    {
                        _transition = new Occurence(null, _cosmos.Time);
                    }
                },
                _cosmos.Time + Durations.Year * 10 * GeometricDistribution.GetNext(_cosmos.Rng, .5 * Trait.Violence.Get(Emnity.Source)));

            if (_transition.HasValue)
                yield return _transition.Value;
        }

        protected override void OnStart()
        {
            var record = new ActionRecord(_cosmos.CreateInstant(), Singleton<Anger>.Instance)
            {
                Objects = { Emnity.Source }
            };
            _feud.Records.Add(record);
            record.Subjects.AddRange(Emnity.Targets);

            Emnity.Source.Records.Add(record);
            foreach (var target in Emnity.Targets)
                target.Records.Add(record);

            if (_cause != null)
                new CausalRelationship(_cause, record).Apply();

            Started.TryAdd(_cosmos.Tracker);
        }
    }
}
