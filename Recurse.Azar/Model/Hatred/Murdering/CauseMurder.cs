﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Model.Hatred;
using Recurse.Azar.Model.Lifecycles;
using Recurse.Azar.Model.People;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Personalities;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Sex.Romances;
using System;
using System.Linq;

namespace Recurse.Azar.Model.Mortality
{
    public static class CauseMurder
    {
        public static ActionRecord Execute(
            Cosmos cosmos,
            Person murderer,
            Person victim,
            Feud feud,
            ActionRecord cause = null)
        {
            // Create a record of the murder
            var record = new ActionRecord(cosmos.CreateInstant(), Singleton<MurderVerb>.Instance)
            {
                Subjects = { murderer },
                Objects = { victim },
                Type = RecordType.Death
            };
            murderer.Records.Add(record);
            victim.Records.Add(record);
            feud.Records.Add(record);
            if (cause != null)
                new CausalRelationship(cause, record).Apply();

            // Execute the death
            CauseDeath.Execute(
                cosmos,
                victim,
                DeathType.Murder,
                record);

            // Mark the murderer
            var isMurderer = murderer.Attributes.OfType<IsMurderer>().SingleOrDefault();
            if (isMurderer == null)
            {
                isMurderer = new IsMurderer();
                murderer.Attributes.Add(isMurderer);
            }
            isMurderer.Victims.Add(victim);
            
            // Make the victim's loved ones angry
            var bereaved = Love.GetTowards(victim);
            foreach (var entry in bereaved)
            {
                var chance =
                    entry.Value *
                    (Entity.GetLocation(new object[] { victim, entry.Key }) != null ? 1 : .5) *
                    Math.Pow(Trait.Violence.Get(entry.Key), 2);
                if (cosmos.Rng.NextDouble() < chance)
                {
                    entry.Key.Events.TryAdd(new LifecycleEvent
                    {
                        Stage = new StartEmnity(
                           cosmos,
                           new Emnity(entry.Key, murderer),
                           feud,
                           record)
                    });
                }
            }

            return record;
        }
    }
}
