﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Utils;
using Recurse.Azar.Language;
using Recurse.Azar.Model.People;
using Recurse.Common.Text;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Records;

namespace Recurse.Azar.Model.Hatred
{
    public class Murder
    {
        private readonly Entity _murderer, _victim;

        public Murder(Entity murderer, Entity victim)
        {
            _murderer = murderer;
            _victim = victim;
        }

        //public override GrammarNoun GetRecordEventNoun(AliasMapping pronouns, PronounType type)
        //{
        //    return
        //        _murderer.GetNoun(pronouns, PronounType.Their).Hyperstring +
        //        "murder of" +
        //        _victim.GetNoun(pronouns, PronounType.Them).Hyperstring;
        //}
    }
}
