﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Azar.Model.Records;
using System;
using System.Collections.Generic;
using Recurse.Common.Text;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.People;
using System.Linq;
using Recurse.Common.Utils;
using Recurse.Azar.Model.Sex.Romances;
using Recurse.Azar.Tracking;

namespace Recurse.Azar.Model.Hatred
{
    public class Emnity
    {
        public readonly Person Source;

        public readonly Person[] Targets;

        public Emnity(Person actor, params Person[] targets)
        {
            if (!targets.Any())
                throw new ArgumentException("targets", "Emnity needs targets.");

            Source = actor;
            Targets = targets;
        }
    }
}
