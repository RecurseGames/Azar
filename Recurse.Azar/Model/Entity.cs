﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Extensions;
using Recurse.Common.Scheduling.Events;
using Recurse.Ianna.Model;
using Recurse.Azar.Language;
using Recurse.Azar.Language.Verbs;
using Recurse.Azar.Model.Attributes;
using Recurse.Azar.Model.Locations;
using Recurse.Azar.Model.People.Records;
using Recurse.Azar.Model.Records;
using Recurse.Azar.Model.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Recurse.RiseAndFall.Model.Locations;

namespace Recurse.Azar.Model
{
    public abstract class Entity
    {
        public readonly CompositeEvent<Time<Duration>> Events = new CompositeEvent<Time<Duration>>();

        public readonly AttributeSet Attributes = new AttributeSet();

        public readonly RecordSet Records = new RecordSet();

        public readonly LocationRecord Location;

        public bool IsDead
        {
            get { return Events.IsFixed; }
        }

        public Entity(bool hasLocation = false)
        {
            Location = hasLocation ? new LocationRecord(this) : null;
        }

        public IGrammarObject GetReference(TextContext context = null)
        {
            if (context == null) return GetSimpleReference();
            var alias = context.Aliases.Get(this);
            if (alias != null) return alias;
            return CreateReference(context);
        }

        public IGrammarObject GetSimpleReference()
        {
            return GetSimpleReference(null);
        }

        public abstract IEnumerable<Entity> GetRelatives();

        public abstract IEnumerable<RecordType> GetRelavantRecords(Entity relative, Time<Duration> time);

        public abstract IGrammarObject GetSimpleReference(AliasMapping aliases);

        protected virtual IGrammarObject CreateReference(TextContext context)
        {
            return GetSimpleReference(context.Aliases);
        }

        public abstract IEnumerable<IRecord> GetIntroduction(TextContext context);

        public abstract IEnumerable<IRecord> TryGetShortIntroduction(TextContext context);

        public static Location GetLocation(IEnumerable items, Time<Duration>? time = null)
        {
            return items
                .Cast<object>()
                .Select(i => i as Entity)
                .Select(e => e != null ? e.Location : null)
                .Select(l => l != null ? (time.HasValue ? l.Get(time.Value) : l.Get()) : null)
                .Distinct()
                .SingleIfAny();
        }

        public static IGrammarObject GetReference<T>(IEnumerable<T> entities, TextContext context) where T : Entity
        {
            if (entities == null) return null;
            return EntityCollection.TryCreate(entities.Cast<Entity>(), context);
        }

        public static IGrammarObject GetSimpleReference<T>(IEnumerable<T> entities, AliasMapping aliases) where T : Entity
        {
            if (entities == null) return null;
            return EntityCollection.TryCreate(entities.Cast<Entity>(), aliases);
        }
    }
}
